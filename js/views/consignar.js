/**
* Consignar Anuncio View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var ConsignarView = {};

	// Extends my object from Backbone events
	ConsignarView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'submit #consignar-anuncio-form': 'consignarAnuncio'

			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this,
					'consignarAnuncio',
					'onCompleteConsignarAnuncio'
				);

				this.consigna = new ConsignarModel();

			}

			/**
			* Validate form anuncio and ajax save
			*
			*/
		,	consignarAnuncio: function( e ){

				e.preventDefault();

				var data = utilities.formToJson( '#consignar-anuncio-form' )
				,	required = [ 
						'nombre',
						'empresa',
						'direccion',
						'ciudad',
						'pais',
						'telefono',
						'correo',
						'equipo',
						'marca',
						'fabricacion',
						'modelo'
					]
				,	errors = [];
				
				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente.', 0 );

				if( ! utilities.justNumbers( data.telefono ) )
					return utilities.showNotification( 'error', 'El campo teléfono solo debe contener números.', 0 );

				if( ! utilities.isEmail( data.correo ) )
					return utilities.showNotification( 'error', 'Digite un correo electrónico valido.', 0 );
				
				this.consigna.consignarAnuncio( data, this.onCompleteConsignarAnuncio, this.onError );
			}


			/**
			* Function when complete the process save anuncio
			*
			*/
		,	onCompleteConsignarAnuncio: function( data ){

				if( data.status == 500 ){
					return utilities.showNotification( 'error', data.message, 0 );
				}

				$('#consignar-anuncio-form')[0].reset();

				return utilities.showNotification( 'success', data.message, 0 );	
				
			}	
		

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.ConsignarView = new ConsignarView();

})( jQuery, this, this.document, this.Misc, undefined );