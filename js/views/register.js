/**
* Crear Anuncio View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var RegisterView = {};

	// Extends my object from Backbone events
	RegisterView = Backbone.View.extend({

			el: $( '.registration' )

		,	events: {
				'click .validate': 'validateTerms'
			}

		,	subviews: {}

		,	view: this

		,	chkArray: []

		,	initialize: function(){

				_.bindAll(
					this,
					'validateTerms'

				);

				this.anuncio = new CrearModel();

			}

			/**
			* Validate form anuncio and ajax save
			*
			*/
		,	validateTerms: function( e ){

				e.preventDefault();

				if( ! $('#jform_terminos').is(':checked')){

					return utilities.showNotification( 'error', 'Acepte los términos y condiciones', 0 );

				}
				
				$('#member-registration').submit();
			}

					

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.RegisterView = new RegisterView();

})( jQuery, this, this.document, this.Misc, undefined );