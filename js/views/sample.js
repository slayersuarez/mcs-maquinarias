/**
* Sample View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var SampleView = {};

	// Extends my object from Backbone events
	SampleView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'submit #analytics-form': 'function'
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this, 
					'function'
				);

				this.model = new SampleModel();

			}

			/**
			* Triggers the click when user saves the analytics configuration
			*
			*/
		,	function: function( e ){

				e.preventDefault();
			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.SampleView = new SampleView();

})( jQuery, this, this.document, this.Misc, undefined );