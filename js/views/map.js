/**
* Google Maps View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var MapView = {};

	// Extends my object from Backbone events
	MapView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'keyup input[ name="localizacion" ]': 'renderAddressOnMap'

			}

		,	view: this

		,	mapOptions: {
				center: new google.maps.LatLng( 4.626730702566083,-74.080810546875  ),
				zoom: 15,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}

		,	map: {}

		,	geocoder: {}

		,	initialize: function(){

				_.bindAll(
					this, 
					'initMap',
					'addListeners',
					'onClickMap',
					'renderAddressOnMap',
					'placeMarker'
				);

				if( $('#map_canvas').length){
					this.initMap();
					this.addListeners();
				}

			}

			/**
			* Initializes the map on the canvas
			*
			*/
		,	initMap: function(){

				this.map = new google.maps.Map(
					document.getElementById("map_canvas"),
					this.mapOptions
				);

				this.geocoder = new google.maps.Geocoder();

				this.marker = new google.maps.Marker({
					map: this.map,
					draggable: false
				});

			}

			/**
			* Add the map listeners to the event
			*
			*/
		,	addListeners: function(){

				google.maps.event.addListener( this.map, 'click', this.onClickMap );

				return;
			}

			/**
			* Catch the map address when user clicks on the map
			*
			*/
		,	onClickMap: function( e ){

				this.placeMarker( e.latLng );
				this.getAddress( e.latLng.k + ',' + e.latLng.B );
			}

			/**
			* Render the address inserted by user on the map
			*
			*/
		,	renderAddressOnMap: function( e ){

				var target = e.currentTarget;

				var address = $( target ).val();

				var _self = this;

				this.geocoder.geocode( { 'address': address}, function(results, status) {

					if (status == google.maps.GeocoderStatus.OK) {

						_self.map.setCenter(results[0].geometry.location);
						_self.marker.setPosition( results[0].geometry.location );

					} else {

						return;
					}
				});
			}

			/**
			* Get the read human address from latitud and longitude
			*
			*/
		,	getAddress: function( latLng ){

				var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latLng + "&sensor=false";

	            jQuery.getJSON(url, function (json) {
	                
	                var formattedAddress = json.results[0].formatted_address;
	                $( 'input[ name="localizacion" ]' ).val( formattedAddress );
	            }); 

			}

			/**
			* Place the marker into the map when user clicks on
			*
			*/
		,	placeMarker: function ( location ){

				this.marker.setPosition( location );
				return;
			}			

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.MapView = new MapView();

})( jQuery, this, this.document, this.Misc, undefined );