/**
* Crear Anuncio View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var CrearView = {};

	// Extends my object from Backbone events
	CrearView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'change #e1': 'selectSubcategorias',
				'submit #crear-anuncio-form': 'saveAnuncio',
				'click #despublicar-anuncio': 'modalDespublicar',
				'submit #despublicar-anuncio-form': 'despublicarAnuncio'
			}

		,	subviews: {}

		,	view: this

		,	chkArray: []

		,	initialize: function(){

				_.bindAll(
					this,
					'saveAnuncio',
					'selectSubcategorias',
					'onCompleteSelectCategorias',
					'render',
					'modalDespublicar',
					'despublicarAnuncio'

				);

				this.anuncio = new CrearModel();

			}

			/**
			* Validate form anuncio and ajax save
			*
			*/
		,	saveAnuncio: function( e ){

				e.preventDefault();

				
				this.subviews.imageUploader = window.imageUploadView;
			

				var data = utilities.formToJson( '#crear-anuncio-form' )
				,	required = [ 
						'titulo',
						'codigo',
						'marca_modelo',
						'anio',
						'pais',
						'codigo_detalle',
						'marca',
						'modelo',
						'anio_detalle',
						'serie',
						'horometro',
						'localizacion',
						'caracteristicas',
						'accesorios',
						'valor'
					]
				,	errors = [];
				
				utilities.validateEmptyFields( required, data, errors );



				data.categoria = $( '#e1' ).select2('val');

				data.subcategoria = $( '#e2' ).select2('val');

				if( data.subcategoria == '' ){
					data.idCategoria = data.categoria;
				}else{
					data.idCategoria = data.subcategoria;
				}

				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente', 0 );

				if( ! utilities.justNumbers( data.valor ) )
					return utilities.showNotification( 'error', 'El campo valor solo debe contener números', 0 );

				if( ! utilities.justNumbers( data.horometro ) )
					return utilities.showNotification( 'error', 'El campo horometro solo debe contener números', 0 );

				if( this.subviews.imageUploader.imageQue.length <= 0  )
					return utilities.showNotification( 'error', 'Debe al menos seleccionar una imagen para su anuncio.', 0 );

				if( $('#e2 > option').length > 1 && data.subcategoria == '' )
					return utilities.showNotification( 'error', 'Debe seleccionar una subcategoria.', 0 );

				if( data.categoria == '')
					return utilities.showNotification( 'error', 'Debe al menos seleccionar una categoria para su anuncio.', 0 );



				data.descripcion_corta = 'Código: ' + data.codigo + ' Marca y Modelo: ' + data.marca_modelo + ' Año:' + data.anio + ' País: ' + data.pais;

				data.descripcion_detallada = encodeURI( this.render( data ) );
				
				data.imagenes = this.subviews.imageUploader.imageQue;


				this.anuncio.set( data );

				this.anuncio.save( this.onCompleteSaveAnuncio, this.onError );

				return;
			}


			/**
			* Function render detalle anuncio
			*
			*/
		,	render: function( data ){

				var html = new EJS( { url: url + 'js/templates/detalle.ejs' } ).render( data );

				return html;

			}


			/**
			* Function when complete the process save anuncio
			*
			*/
		,	onCompleteSaveAnuncio: function( data ){


				if( data.status == 500 ){
					return utilities.showNotification( 'error', data.message, 0 );
				}

				$('#crear-anuncio-form')[0].reset();

				if( ! data.validateCoupon ){
					$('#form-payu').html(data.form);

					$( '#form-pagos' ).submit();

					return utilities.showNotification( 'success', data.message, 0 );
				}

				utilities.showNotification( 'success', data.message, 100 );
				return utilities.redirect( 'anuncio' ); 
				
				
			}	

			/**
			* Render subcategorias by categoria
			*
			*/
		,	selectSubcategorias: function(){

				var idCategoria = $('#e1').val();

				this.anuncio.set();

				this.anuncio.getSubcategorias( idCategoria, this.onCompleteSelectCategorias, this.onError );
				
			}

		,	onCompleteSelectCategorias: function( data ){

				if( data.status == 500 ){
					$( '#e2' ).find('option:gt(0)').remove();
					$("#e2").trigger("change");
					return utilities.showNotification( 'error', data.message, 0 );
				}

				$( '#e2' ).find('option:gt(0)').remove();
				$( "#e2" ).trigger("change");

				$.each(data.subcategorias, function(index, item) {

					var html = "<option value="+item.virtuemart_category_id+">"+item.category_name+"</option>";

					$(html).appendTo('#e2');

				});



			}

			/**
			* Despublicar anuncio
			*
			*/
		,	modalDespublicar: function( e ){

				e.preventDefault();

				var _this = this;

				var data = {}

				if( $(".mcs-check:checked").length <= 0 )
					return utilities.showNotification( 'error', 'Seleccione un anuncio.', 0 );
				
	
				/* look for all checkboxes that have a class 'chk' attached to it and check if it was checked */
				$(".mcs-check:checked").each(function() {

					_this.chkArray.push($(this).val());

				});

				data.idsAnuncio = this.chkArray;

				var html = new EJS( { url: url + 'js/templates/despublicar.ejs' } ).render( data );

				var modal = {
					title: 'Despublicar anuncio',
					content: html

				};

				utilities.showModalWindow( modal );

			}

		,	despublicarAnuncio: function( e ){

				e.preventDefault();

				var data = utilities.formToJson( '#despublicar-anuncio-form' )
				,	required = [ 
						'motivo'
					]
				,	errors = [];
				
				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el motivo de la despublicación.', 0 );

				this.anuncio.despublicarAnuncio( this.chkArray, data.motivo, this.onCompleteDespublicarAnuncio, this.onError );

			}


		,	onCompleteDespublicarAnuncio: function( data ){


				if (data.status == 500)
					return utilities.showNotification( 'error', data.message , 0 );

				$('#despublicar-anuncio-form')[0].reset();

				utilities.showNotification( 'success', data.message, 0 );	

				return utilities._closeModalWindow();

			}

					

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.CrearView = new CrearView();

})( jQuery, this, this.document, this.Misc, undefined );