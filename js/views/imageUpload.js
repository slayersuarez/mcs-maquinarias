/**
* Image Uploader View
* version : 1.0
* package: casainmobiliaria.frontend
* package: casainmobiliaria.frontend.mvc
* author: 
* Creation date: August 2014
*
* Allows to upload images for ads in Virtuemart
*
*/
( function( $, window, document, Utilities ){	

	// Create a var to manage the events
	var imageUploadView = {};

	// Extends my object from Backbone events
	imageUploadView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click .delete-link': 'onRemove'
			}		

		,	_self: this

		,	allowToUpload: false

		,	uploader: {}

		,	imageQue: []

		,	imageCount: 0

		,	sliderOptions : {
				responsive: true,
				slideMargin: 10,
				slideWidth: 200,
				minSlides: 3,
				maxSlides: 3
			}

		,	initialize: function(){

				_.bindAll(
					this,
					'onSubmit',
					'onUpload',
					'onRemove',
					'onComplete',
					'onCompleteRemove',
					'editDescription'
				);

				this.imageParams = {

						element: document.getElementById('image-uploader')
					,	uploadButtonText: 'Cargar imágenes'	
					,   action: '../index.php'
					,   params: {

					    	option: 'com_anuncio',
					    	task: 'anuncio.uploadImages'
					    }

					,   allowedExtensions: [ 'jpg', 'png', 'jpeg' ]
					,   debug: false
					,	onSubmit: this.onSubmit
					,	onUpload: this.onUpload
					,	onComplete: this.onComplete
					,   onError: this.onError
				    
				}
				if( $('#image-uploader').length ){
					this.uploader = new qq.FileUploader( this.imageParams );
				}

				this.imageUploadModel = new imageUploadModel();
			}

			/**
			*
			*/
		,	onSubmit: function( id, fileName ){

				var queue = $( "input[name='file']" )[0].files;

				if( this.imageCount == 0 )
					this.imageCount = queue.length;

			}

		,	onUpload: function( id, filename, xhr ){

				
			}

			/**
			*
			*/
		,	onProgress: function( id, fileName, loaded, total ){

				var queue = $( "input[name='file']" )[0].files;

				if ( queue.length < 3 || queue.length > 10 ) {
					this.uploader._handler.cancelAll();
					return Utilities.showNotification('error', 'sumadre', 0);
				}

				var perc = Math.floor( loaded / total * 100 );
			}

			/**
			*
			*/
		,	onComplete: function(id, fileName, responseJSON){


				if( ! responseJSON.success ){
					$( '.qq-upload-list' ).remove();
					return;
				}

				
				var li = $( '<li>' )
				,	img = $( '<img>' );

				if( $( 'ul.bxslider' ).length <= 0 ){
					var ul = $( '<ul>' );
					ul.addClass('bxslider');
					$( '.content-images' ).append( ul );
					this.slider = $('.bxslider').bxSlider(this.sliderOptions);
				}

				li.attr( 'id', responseJSON.filename );
				img.attr( 'src',  url +'imganuncios/tmp/'+ responseJSON.filename );
				// linkDel.addClass('delete-link')
				// 	   .text('x')
				// 	   .attr( {
				// 			'href': '#',
				// 			'data-img': responseJSON.filename,

				// 		});	
				
				li.append(img);
				// li.append(linkDel);
				$( '.content-images .bxslider' ).append(li);	
				$( '.qq-upload-list' ).remove();

				this.imageCount--;

				if( this.imageCount <= 0 )
					this.slider.reloadSlider( this.sliderOptions );

				this.imageQue.push( responseJSON.filename );
				


			}				

			/**
			*
			*/
		,	onRemove: function( e ){

				e.preventDefault();

				var target = e.currentTarget;
				var parentLi = $( target ).parent('li');
	
				var _data = {
						'filename': $( target ).data( 'img' )
					,	'imgindex': $( ".content-images > ul > li" ).index( parentLi )
				};				

				this.imageUploadModel.delete( _data, this.onCompleteRemove, this.onError() );
			}

			/**
			*
			*/
		,	onCompleteRemove: function( data ){

				if( data.success ){

					$( '#' + data.filename ).remove();
					this.slider.reloadSlider( this.sliderOptions );
					return;
				}

				return Utilities.showNotification( 'error', 'No se pudo eliminar la imagen '+ data.filename , 0 );				
			}


		,	editDescription: function () {
				

					$( '.product_s_desc' ).each(function( index ) {

						var introText = $(this).text();

						introText = introText.split('-');

						var li = $( '<li>' )
						,	ul = $( '<ul>' );

						ul.addClass('introText');

						li.append( '<span>' + introText[0] + '</span><br>' );
						li.append( '<span>' + introText[1] + '</span><br>' );
						li.append( '<span>' + introText[2] + '</span>' );

						ul.append( li );

						$( this ).parent().append( ul );

						$( this ).remove();
						
					});
					
			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				Utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}


	});	

	$(document).ready(function($) {
		window.imageUploadView = new imageUploadView();
		window.imageUploadView.editDescription();
	});		

})( jQuery, this, this.document, this.Misc, 'undefined' );