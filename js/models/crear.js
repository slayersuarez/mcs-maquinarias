/**
* Crear Anuncio Model
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	CrearModel = Backbone.Model.extend({
	        defaults: {
		            id: 0,
		            titulo: '',
		            descripcion_corta: '',
		            descripcion_detallada: '',
		            valor: '',
		            imagenes: [],
		            idCategoria: 0,
		            codigo_activacion: ''
	        }

	    ,   initialize: function(){

	    		
	            
	        }

	        /**
	        * Gets the subcategorias from id
	        *
	        */
	    ,	getSubcategorias: function( data, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncio'
					,	task: 'anuncio.getSubcategoria'
					,	data: data

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}


	    	/**
	    	* Saves the anuncio
	    	*
	    	* @param { function } the callback function
	    	* @param { function } the callback error function
	    	*
	    	*/
	    ,	save: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncio'
					,	task: 'anuncio.save'
					,	anuncio: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );
	    	}

	    /**
	    	* get code activation
	    	*
	    	* @param { function } the callback function
	    	* @param { function } the callback error function
	    	*
	    	*/
	    ,	despublicarAnuncio: function( data, motivo, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_anuncio'
					,	task: 'anuncio.despublicarAnuncio'
					,	data: data
					,	motivo: motivo

				}



				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );
	    	}
    });

})( jQuery, this, this.document, this.Misc, undefined );