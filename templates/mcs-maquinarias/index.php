<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <?php JHTML::_('behavior.mootools'); ?>

        <!-- Joomla Head -->
        <jdoc:include type="head" />

        <!-- Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Michroma' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <!-- Less -->
        <link rel="stylesheet/less" type="text/css" href="load-styles.php?load=template"/>
        
        <!-- Script -->

        <!-- Script -->
        <script type="text/javascript" src="js/libs/less.min.js"></script>


        <script>
        var canvas, stage, exportRoot;

        function init() {
            canvas = document.getElementById("canvas");
            exportRoot = new lib.logo();

            stage = new createjs.Stage(canvas);
            stage.addChild(exportRoot);
            stage.update();
            stage.enableMouseOver();

            createjs.Ticker.setFPS(lib.properties.fps);
            createjs.Ticker.addEventListener("tick", stage);
        }
        </script>

    </head>

    <body onload="init();" style="background-color:transparent;">

            <!--CABEZOTE DE LA PAGINA-->
        
        <header>

            <div class="onepcssgrid-1200">

                <div class="onerow header" >

                    <div class="col4 top">
                        <div class="logo">
                            <canvas id="canvas" width="380" height="77" style="background-color:transparent;"></canvas>
                            <img src="images/logo.png">
                        </div>  
                    </div> 

                    <div class="col3 last top">  
                        <div class="ditribuidor">
                            <jdoc:include type="modules" name="distribuidor" style="xhtml" /> 
                        </div>
                    </div>

                    <div class="col5 last top">  
                        <div class="banner">
                            <jdoc:include type="modules" name="banner" style="xhtml" /> 
                        </div>
                    </div>
                </div>
            </div>

        </header> 

                        <!-- Menu larger screens-->

        <div class="content-nav">

            <div class="onepcssgrid-1200">

                <div class="onerow nav" >

                    <div class="col12 last top">

                        <nav class="menu-larger-screens">

                            <ul>  
                                <li><a href="#">Comprar</a></li>  
                                <li><a href="#">Colocar anuncio</a></li>   
                                <li><a href="#">Servicios</a> 
                                <li><a href="#">Valor agregado</a></li> 
                                <li><a href="#">Aliados</a></li>  
                                <li><a href="#">Contácto</a></li>    
                            </ul>

                        </nav>  
                        
                        <!-- Menu Mobile screens-->
                        
                        <nav class="menu-mobile-screens">

                            <a href="#" id="pull">Menú</a> 

                            <ul>
                                <li><a href="#">Comprar</a></li>  
                                <li><a href="#">Colocar anuncio</a></li>   
                                <li><a href="#">Servicios</a> 
                                <li><a href="#">Valor agregado</a></li> 
                                <li><a href="#">Contácto</a></li>  
                                <li><a href="#">Quiénes somos</a></li>    
                            </ul>

                        </nav> 

                    </div>

                </div>

            </div>

        </div>

            <!--CONTENIDO DE LA PAGINA-->

        <main>
         
            <seccion class="one">

                <div class="onepcssgrid-1200">

                    <div class="onerow bienvenida">
                        <div class="col12 last bienvenida">
                            <jdoc:include type="modules" name="bienvenida" style="xhtml" />  
                        </div>
                    </div> 

                    <div class="onerow registrate" >
                        <div class="col12 registrate">
                            <jdoc:include type="modules" name="registrate" style="xhtml" />
                        </div>
                    </div>

                    <div class="onerow buscar" >
                        <div class="col12 buscar">
                            <jdoc:include type="modules" name="buscar" style="xhtml" />
                        </div>
                    </div>

                    <div class="onerow ofertas" >
                        <div class="col12 ofertas">
                            <jdoc:include type="modules" name="ofertas" style="xhtml" />
                        </div>
                    </div> 

                </div>  

            </seccion>

            <seccion class="two">

                <div class="onepcssgrid-1200">

                    <div class="onerow content-main">

                        <div class="col4 last productos">

                            <div class="menu-izq">
                                <jdoc:include type="modules" name="menu-izq" style="xhtml" />  
                            </div>

                        </div>

                        <div class="col5 last productos">

                            <div class="menu-der-1">
                                <jdoc:include type="modules" name="menu-der-1" style="xhtml" />  
                            </div>
                            <div class="menu-der-2">
                                <jdoc:include type="modules" name="menu-der-2" style="xhtml" />  
                            </div>
                            <div class="menu-der-3">
                                <jdoc:include type="modules" name="menu-der-3" style="xhtml" />  
                            </div>

                        </div>

                        <div class="col3 last productos">
                           
                            <div class="clientes">
                                <jdoc:include type="modules" name="clientes" style="xhtml" />  
                                <p>Distribuidores</p>
                            </div>

                            <div class="clientes-2">
                                <jdoc:include type="modules" name="clientes-2" style="xhtml" /> 
                                <p>Enlaces<br/> de interés</p> 
                            </div>

                            <div class="clientes-3">
                                <jdoc:include type="modules" name="clientes-3" style="xhtml" /> 
                            </div>

                        </div>

                    </div> 

                </div> 

                <div class="nav-bottom">
                    <jdoc:include type="modules" name="nav-bottom" style="xhtml" />  
                </div>

            </seccion>

            <seccion class="three">

                <div class="onepcssgrid-1200">

                    <div class="col12 last paises">
                        <jdoc:include type="modules" name="paises" style="xhtml" />  
                    </div>

                </div>

            </seccion>

            <seccion class="four">

                <div class="onepcssgrid-1200">

                    <div class="col6 last redes">
                         <jdoc:include type="modules" name="redes" style="xhtml" />
                    </div>

                    <div class="col6 last pagos">
                        <jdoc:include type="modules" name="pagos" style="xhtml" />  
                    </div>

                </div>

            </seccion>

        </main>  

                <!--PIE DE LA PAGINA-->

        <footer>

            <seccion class="five">

                <div class="onepcssgrid-1200">

                    <div class="col12 last nav-footer">
                        <jdoc:include type="modules" name="nav-footer" style="xhtml" />  
                    </div>

                </div>

            </seccion>

            <seccion class="six">

                <div class="onepcssgrid-1200">

                    <div class="col6 last pie">
                        <jdoc:include type="modules" name="pie" style="xhtml" />  
                    </div>

                    <div class="col6 last pie">
                        <div class="derechos">
          
                              <div class="content-derechos">
                              
                                <div id="copy"><span class="sainet">Sitio Web Desarrollado Por</span>
                                  <a target="_blank" href="http://www.creandopaginasweb.com/">
                                      <div id="logo">
                                      </div>
                                  </a>
                                </div>
                              </div>
                      </div>
                    </div>

                </div>

            </seccion>
                   
        </footer>

        <script src="http://code.createjs.com/easeljs-0.7.1.min.js"></script>
        <script src="http://code.createjs.com/tweenjs-0.5.1.min.js"></script>
        <script src="http://code.createjs.com/movieclip-0.7.1.min.js"></script>
        <script src="http://code.createjs.com/preloadjs-0.4.0.min.js"></script>
        <script src="html5/logo.js"></script>
        <script src="html5/menu.js"></script>




    </body>
</html>