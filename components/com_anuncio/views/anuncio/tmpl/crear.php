<?php

/**
* Default Template for Mis Anuncios
*
*/

// Initialize
defined('_JEXEC') or die;
$app = JFactory::getApplication(); // Joomla application
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root

$document = JFactory::getDocument();

?>



<script>
    $(document).ready(function() { $("#e1").select2(); $("#e2").select2(); });
</script>

<div class="onerow">
	<div class="col12 last crear-view">
		<a href="index.php/anuncio" class="mcs-button medium red iconic rounded shadow"><i class="fa fa-bullhorn"></i><span>Mis Anuncios</span></a>
	</div>
</div>

<div class="onerow">
	<div class="col12">
		<ul class="mcs-tabs col12">
			<li class="active"><a href="#">Crear anuncio</a></li>
			<li><a href="index.php/anuncio?view=anuncio&layout=consignar">Consignar equipo</a></li>
		</ul>
	</div>
</div>

<div class="onerow">
	<div class="col12 crear-anuncio-content">
		<h3 class="mcs-title red">Crear un nuevo anuncio</h3>

		<span class="mcs-user">Los campos marcados con <span class="star">* Campos obligatorios</span></span>
	</div>
</div>

<div class="onerow">
	<form class="forms" id="crear-anuncio-form">
		<div class="col6 datos-anuncio">
			<h3 class="mcs-title orange">Datos del anuncio</h3>

			<ul>
				<li><label class="mcs-user">Titulo del anuncio</label></li>
				<li><input type="text" name="titulo"></li>
			</ul>

			<h3 class="mcs-title red border">Descripción corta</h3>

			<ul class="two-columns">
				<li>
					<label class="mcs-user">Código
						<span class="star">*</span>
					</label>
					<input type="text" name="codigo">
				</li>

				<li>
					<label class="mcs-user">Marca y Modelo
						<span class="star">*</span>
					</label>
					<input type="text" name="marca_modelo">
				</li>

				<li>
					<label class="mcs-user">Año
						<span class="star">*</span>
					</label>
					<input type="text" name="anio">
				</li>

				<li>
					<label class="mcs-user">País
						<span class="star">*</span>
					</label>
					<input type="text" name="pais">
				</li>
			</ul>

			<h3 class="mcs-title red border">Descripción detallada</h3>

			<ul class="two-columns">
				<li>
					<label class="mcs-user">Código
						<span class="star">*</span>
					</label>
					<input type="text" name="codigo_detalle">
				</li>

				<li>
					<label class="mcs-user">Marca
						<span class="star">*</span>
					</label>
					<input type="text" name="marca">
				</li>

				<li>
					<label class="mcs-user">Modelo
						<span class="star">*</span>
					</label>
					<input type="text" name="modelo">
				</li>

				<li>
					<label class="mcs-user">Año
						<span class="star">*</span>
					</label>
					<input type="text" name="anio_detalle">
				</li>

				<li>
					<label class="mcs-user">Serie
						<span class="star">*</span>
					</label>
					<input type="text" name="serie">
				</li>

				<li>
					<label class="mcs-user">Horómetro
						<span class="star">*</span>
					</label>
					<input type="text" name="horometro">
				</li>	

				<li>
					<label class="mcs-user">Localización, Ciudad, País
						<span class="star">*</span>
					</label>
					<input type="text" name="localizacion">
					<div id="map_canvas" style="width:100%; height:200px"></div>
				</li>

				<li>
					<label class="mcs-user">Características: Potencia, capacidad, balde
						<span class="star">*</span>
					</label>
					<textarea name="caracteristicas"></textarea>
				</li>

				<li>
					<label class="mcs-user">Accesorios
						<span class="star">*</span>
					</label>
					<textarea name="accesorios"></textarea>
				</li>
			</ul>

			<h3 class="mcs-title red border">Valor</h3>

			<ul>
				<li>
					<label class="mcs-user">Valor
						<span class="star">*</span>
					</label>
				</li>
				<li><input type="text" name="valor"></li>
			</ul>

		</div>

		<div class="col6 last">
			<div class="cargar-images">
				<h3 class="mcs-title orange">Imágenes del anuncio</h3>
				<div class="wrapper-images-uploader">
		           <div id="image-uploader">       
		               <noscript>          
		                   <p>Please enable JavaScript to use file uploader.</p>
		                   <!-- or put a simple form for upload here -->
		               </noscript>         
		           </div>
		           <div class="content-images content-multimedia">
		               <!-- <ul class="bxslider"></ul> -->
		           </div>
		       </div>
			</div>

			<div class="categorias">
				<h3 class="mcs-title orange">Categorias</h3>

				<ul>
					<li>
						<label class="mcs-user">Categoria principal
							<span class="star">*</span>
						</label>
					</li>
					<li>
						<select id="e1" class="mcs-select" name="categoria">
							<option value="">Seleccione</option>
							<?php foreach ($this->items as $key => $item) {
							?>
								<option value="<?php echo $item->virtuemart_category_id; ?>"><?php echo $item->category_name; ?></option>
							<?php
							} ?>
							
						</select>
					</li>
					<li><span class="mcs-user">Categoría secundaria</span></li>
					<li>

						<select id="e2" class="mcs-select" name="subcategoria">
							<option value="">Seleccione</option>
						</select>
					</li>
				</ul>
			</div>

			<div class="activacion">
				<h3 class="mcs-title orange">Código de activación</h3>

				<ul>
					<li><span class="mcs-user">Ingresar código de activación</span></li>
					<li>
						<input type="text" name="codigo_activacion">
					</li>
				</ul>
			</div>

			<div class="buttons">
				<ul>
					<li><input type="submit" class="mcs-button big red" value="Crear Anuncio"/></li>
					<li><input type="submit" class="mcs-button big gray" value="Cancelar"/></li>
				</ul>
			</div>
		</div>
		
	</form>
		<div id="form-payu"></div>
</div>