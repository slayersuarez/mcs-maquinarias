<?php

/**
* Default Template for Mis Anuncios
*
*/

// Initialize
defined('_JEXEC') or die;
$app = JFactory::getApplication(); // Joomla application
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root

$document = JFactory::getDocument();


?>

<div class="onerow">
	<div class="col12 last crear-view">
		<a href="index.php/anuncio?view=anuncio&layout=crear" class="mcs-button medium red iconic rounded shadow"><i class="fa fa-bullhorn"></i><span>Crear Anuncio</span></a>
	</div>
</div>

<div class="onerow">
	<div class="col12 crear-anuncio-content">
		<h3 class="mcs-title red">Mis anuncios</h3>

		<button type="submit" class="mcs-button medium orange iconic rounded" id="despublicar-anuncio"><i class="fa fa-trash-o"></i><span>Despublicar</span></button>
	</div>
</div>

<div class="onerow">
	<div class="col12 lista-anuncios">

		<hr>
		
		<ul class="anuncios">
			
			<?php 
				if ( empty( $this->anuncios ) ){
			?>
					<div class="mcs-tooltip warning">
						Usted aún no ha creado anuncios
					</div>
			<?php
				}
					
				foreach ($this->anuncios as $key => $anuncio) {
			?>
					<li>
						<input class="mcs-check" type="checkbox" id="check<?php echo $anuncio->virtuemart_product_id ?>" value="<?php echo $anuncio->virtuemart_product_id ?>"/>
						<label for="check<?php echo $anuncio->virtuemart_product_id ?>"></label>
						<img src="images/stories/virtuemart/product/<?php echo $anuncio->file_title ?>">
						<h3 class="mcs-title red"><?php echo $anuncio->product_name ?></h3>
						<p><?php echo $anuncio->product_s_desc ?></p>

					</li>

			<?php
				} 
			?>
		</ul>

		<div class="wrapper-paginator">
			<?php echo $this->upcomingPaginator; ?>
		</div>
	</div>
</div>

