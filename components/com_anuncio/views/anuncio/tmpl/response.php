<?php

/**
* Default Template for Mis Anuncios
*
*/

// Initialize
defined('_JEXEC') or die;
$app = JFactory::getApplication(); // Joomla application
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root

$document = JFactory::getDocument();

?>

<div class="onerow">
	<div class="col12 last crear-view">
		<a href="index.php/anuncio" class="mcs-button medium red iconic rounded shadow"><i class="fa fa-bullhorn"></i><span>Mis Anuncios</span></a>
	</div>
</div>


<div class="onerow">
	<div class="col12 last crear-view">
		 <?php 

        if($_REQUEST['transactionState'] == 6 && $_REQUEST['polResponseCode'] == 5){
    ?>
            <div class="mcs-tooltip error">
                <h2>Transacción fallida</h2>
            </div>
    <?php  
        }else if($_REQUEST['transactionState'] == 6 && $_REQUEST['polResponseCode'] == 4){
    ?>
            <div class="mcs-tooltip error">
                <h2>Transacción rechazada</h2>
            </div>
    <?php
           
        }else if($_REQUEST['transactionState'] == 12 && $_REQUEST['polResponseCode'] == 9994){

    ?>
           <div class="mcs-tooltip warning">
                <h2>Pendiente, Por favor revisar si el débito fue realizado en el Banco</h2>
            </div>
    <?php
            
        }else if($_REQUEST['transactionState'] == 4 && $_REQUEST['polResponseCode'] == 1){
    ?>
            <div class="mcs-tooltip success">
                <h2>Transacción aprobada</h2>
            </div>
    <?php
        }else{
    ?>
            <div class="mcs-tooltip error">
                <h2><?php echo $_REQUEST['mensaje']; ?></h2>
            </div>
    <?php
            
        }

    ?>
	</div>
</div>