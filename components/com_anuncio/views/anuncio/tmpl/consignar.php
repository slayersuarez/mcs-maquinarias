<?php

/**
* Default Template for Mis Anuncios
*
*/

// Initialize
defined('_JEXEC') or die;
$app = JFactory::getApplication(); // Joomla application
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root

$document = JFactory::getDocument();

?>

<div class="onerow">
	<div class="col12 last crear-view">
		<a href="index.php/anuncio" class="mcs-button medium red iconic rounded shadow"><i class="fa fa-bullhorn"></i><span>Mis Anuncios</span></a>
	</div>
</div>

<div class="onerow">
	<div class="col12">
		<ul class="mcs-tabs col12">
			<li><a href="index.php/anuncio?view=anuncio&layout=crear">Crear anuncio</a></li>
			<li class="active"><a href="#">Consignar equipo</a></li>
		</ul>
	</div>
</div>

<div class="onerow">
	<div class="col12 crear-anuncio-content">
		<h3 class="mcs-title red">Consignar equipo</h3>

		<span class="mcs-user">Los campos marcados con <span class="star">* Campos obligatorios</span></span>
	</div>
</div>

<div class="onerow">
	<form class="forms" id="consignar-anuncio-form">
		<div class="col6 datos-consignar">
			<h3 class="mcs-title orange">Datos básicos</h3>

			<ul class="two-columns">
				<li>
					<label class="mcs-user">Nombre
						<span class="star">*</span>
					</label>
					<input type="text" name="nombre">
				</li>

				<li>
					<label class="mcs-user">Nombre de la Empresa
						<span class="star">*</span>
					</label>
					<input type="text" name="empresa">
				</li>

				<li>
					<label class="mcs-user">Dirección
						<span class="star">*</span>
					</label>
					<input type="text" name="direccion">
				</li>

				<li>
					<label class="mcs-user">Ciudad
						<span class="star">*</span>
					</label>
					<input type="text" name="ciudad">
				</li>
			
				<li>
					<label class="mcs-user">País
						<span class="star">*</span>
					</label>
					<input type="text" name="pais">
				</li>

				<li>
					<label class="mcs-user">Teléfono
						<span class="star">*</span>
					</label>
					<input type="text" name="telefono">
				</li>

				<li>
					<label class="mcs-user">Email
						<span class="star">*</span>
					</label>
					<input type="text" name="correo">
				</li>
			</ul>

		</div>

		<div class="col6 last">

			<div class="equipo">
				<h3 class="mcs-title orange">Datos del equipo</h3>

				<ul class="two-columns">
					<li>
						<label class="mcs-user">Equipo o camión consignar
							<span class="star">*</span>
						</label>
						<input type="text" name="equipo">
					</li>

					<li>
						<label class="mcs-user">Marca
							<span class="star">*</span>
						</label>
						<input type="text" name="marca">
					</li>

					<li>
						<label class="mcs-user">Año de Fabricación
							<span class="star">*</span>
						</label>
						<input type="text" name="fabricacion">
					</li>

					<li>
						<label class="mcs-user">Modelo o Referencia
							<span class="star">*</span>
						</label>
						<input type="text" name="modelo">
					</li>
				
				</ul>
				
			</div>

			<div class="buttons">
				<ul>
					<li><input type="submit" class="mcs-button big red" value="Enviar"/></li>
					<li><input type="submit" class="mcs-button big gray" value="Cancelar"/></li>
				</ul>
			</div>

			
		</div>
		
	</form>
		<div id="form-payu"></div>
</div>