<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class AnuncioViewAnuncio extends JViewLegacy {

	protected $items;
	public $anuncios;
	public $upcomingPaginator;
	public $totalUpcomings;
	
	// Function that initializes the view
	function display( $tpl = null ){
	
		$this->items = $this->get( 'Categorias' );

		$this->listAnuncios();
		$this->getTotalUpcomings();

		parent::display( $tpl );
	
	}

	public function listAnuncios(){

		$user = JFactory::getUser();

		$idUser = $user->get('id');

		$offset = JRequest::getVar( 'offset' );
		$active = JRequest::getVar( 'page' );

		$offset = ( is_numeric( $offset ) ) ? $offset : 0;
		$active = ( is_numeric( $active ) ) ? $active : 1;

		$model = new AnuncioModelAnuncio();

		$this->anuncios = $model->getAnuncios( $idUser, $offset, 4 );

		$this->totalUpcomings = $this->getTotalUpcomings();

		$pages = ceil($this->totalUpcomings/4);


		$this->upcomingsPaginator($pages,$active);
	}


	/**
	* Gets total anuncios
	*
	*/
	public function getTotalUpcomings(){

		$user = JFactory::getUser();

		$idUser = $user->get('id');

		$upcomings = array();

		$model = new AnuncioModelAnuncio();

		$anuncios = $model->getAnuncios( $idUser );

		$total =  count($anuncios);

		return $total;

	}

	/**
	* Prints user paginator
	*
	*/
	protected function upcomingsPaginator($pages, $active){


		$url = JFactory::getURI();
		$url = $url->root();

			if( $pages > 1 ){

				$next = $active + 1;
				$next = ( $next > $pages ) ? $pages : $next;

				$offsetNext = ( $next == 1 ) ? 0 : ( $next - 1 ) * 4;

				$prev = $active - 1;
				$prev = ( $prev < 1 ) ? 1 : $prev;

				$offsetPrev = ( $prev == 1 ) ? 0 : ( $prev - 1 ) * 4;
				
				$paginator .= '<ul>
				<li>
					<a href="index.php/anuncio/?offset=' . $offsetPrev . '&page=' . $prev . '" class="mcs-arrows left">
					<i class="fa fa-long-arrow-left"></i>
					<span>Atrás</span>
					</a>
				</li>';

						for ( $i = 1; $i <= $pages; $i++ ):

							$offset = ( $i == 1 ) ? 0 : ( $i - 1 ) * 4;
							$class = ( $i == $active ) ? 'class="active"' : '';

							$paginator .= '<li ' . $class . '><a href="index.php/anuncio/?&offset=' . $offset . '&page=' . $i . '">' . $i . '</a></li>';
						endfor;
				$paginator .= '<li>
				<a href="index.php/anuncio/?offset=' . $offsetNext . '&page=' . $next . '" class="mcs-arrows right" >
					<i class="fa fa-long-arrow-right"></i>
					<span>Siguiente</span>
				</a></li></ul>';
			}

		$this->upcomingPaginator = $paginator;
	}

}
?>