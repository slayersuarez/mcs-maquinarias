<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class AnuncioControllerConsignar extends JController{


	public function sendMail(){

		$data = JRequest::getVar( 'data' );


		$app = JFactory::getApplication();

		$mailAdmin = $app->getCfg('mailfrom');

		$response = new stdClass();

		$mail = JFactory::getMailer();

		
        $contenido = "<div><img src='".JURI::root()."images/header-mail.png'><br><br><br><br>";
        $contenido .= "Nombre: ".$data['nombre']."<br>";
        $contenido .= "Nombre de la empresa: ".$data['empresa']."<br>";
       	$contenido .= "Dirección: ".$data['direccion']."<br>";
       	$contenido .= "Ciudad: ".$data['ciudad']."<br>";
       	$contenido .= "País: ".$data['pais']."<br>";
       	$contenido .= "Teléfono: ".$data['telefono']."<br>";
       	$contenido .= "Equipo o camión consignar: ".$data['equipo']."<br>";
       	$contenido .= "Marca: ".$data['marca']."<br>";
       	$contenido .= "Año de Fabricación: ".$data['fabricacion']."<br>";
       	$contenido .= "Modelo o Referencia: ".$data['modelo']."<br>";
        $contenido .= "<img src='".JURI::root()."images/footer-mail.png'></div>";

        $mail->setSender( $data['correo']);
        $mail->addRecipient( $mailAdmin );
        $mail->setSubject( $data['nombre']. " desea consignar un anuncio." );
        $mail->Encoding = 'base64';
        $mail->isHtml( true );
        $mail->setBody( $contenido );

        if ( ! $mail->Send() ) {
        	$response->status = 500;
			$response->message = 'Su anuncio no pudo ser enviado.';

			echo json_encode($response);
			die;
        }


		$response->status = 200;
		$response->message = 'Se han enviado los datos para la consignación de su equipo. Pronto lo contactáremos para iniciar el proceso.';

		echo json_encode($response);
		die;

	}
	

}
?> 