<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class AnuncioControllerAnuncio extends JController{

	/**
	*
	* Uploads and parse and Image file
	*
	*/
	public function uploadImages(){

		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'jpg', 'png', 'jpeg' );
		// max file size in bytes
		$sizeLimit = 2 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('imganuncios/tmp/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}

	/**
	*
	* Eliminar una imagen
	*
	*/
	public function deleteImage(){

		$requestData = JRequest::getVar( 'data', array() );	
		$response = new stdClass();
		$pathImg = 'imganuncios/tmp/'.$requestData[ 'filename' ];

		if( file_exists($pathImg) ){

		unlink($pathImg);
		$response->success = true;
		$response->filename = $requestData[ 'filename' ];
		$response->imgindex = $requestData[ 'imgindex' ];

		echo json_encode($response);
		die;
		}

		$response->success = false;
		$response->filename = $requestData[ 'filename' ];
		$response->imgindex = $requestData[ 'imgindex' ];

		echo json_encode($response);
		die;
	}

	public function getSubcategoria(){

		


		$data = JRequest::getInt('data');
		$response = new stdClass();

		$model = $this->getModel( 'anuncio' );

		$subcategorias = $model->getSubcategorias( $data );

		if( empty($subcategorias) ){
			$response->status = 500;
			$response->message = 'La categoría seleccionada no tiene subcategorias';

			echo json_encode($response);
			die;
		}

		$response->status = 200;
		$response->subcategorias = $subcategorias;

		echo json_encode($response);
		die;
	}

	public function save(){

		$data = JRequest::getVar('anuncio');
		$response = new stdClass();

		$codigo = $this->validarCodigo( $data['codigo_activacion'] );


		if( ! empty($codigo) ){
			$published = 1;
		}else{
			$published = 0;
		}

		$producto = $this->getModel('producto');

		$user = JFactory::getUser();

		$idUser = $user->get('id');


		$date = date( 'Y-m-d H:i:s' );

		$sku = md5(uniqid());

		$args = array(
				'product_available_date' => $date
			,	'published' => $published
			,	'created_on' => $date
			,	'created_by' => $idUser
			,	'modified_on' => $date
			,	'modified_by' => $idUser
			,	'product_sku' => $sku
		);

		$producto->instance( $args );

		if(! $producto->save('bool')){
			$response->status = 500;
			$response->message = 'Anuncio no guardado.';

			echo json_encode($response);
			die;

		}

		$slugProducto = Misc::parseNomField( $data['titulo'] );	

		$idProducto = $producto->insertId;

		$descripcionDetallada = urldecode( $data['descripcion_detallada'] );

		$productoNombre = array(
				$idProducto
			,	$data['descripcion_corta']
			,	$descripcionDetallada
			,	$data['titulo']
			,	$slugProducto
		);

		$producto->saveNombre( $productoNombre );

		$productoCategoria = array(
			$idProducto,
			$data['idCategoria']
		);

		$producto->mergeCategoria( $productoCategoria );

		$productoPrecio = array(
			$idProducto,
			$data['valor'],
			$date,
			$idUser
		);

		$producto->mergePrecio( $productoPrecio );

		$imagenes = $this->saveImages( $data['imagenes'], $date, $idUser );
		$this->mergeImagenes( $idProducto, $imagenes );
		

		$mail = $user->get('email');

		// get costo anuncio from admin
		$app = JFactory::getApplication();
		$costoAnuncio = $app->getCfg('costo_anuncio');

		if( empty( $codigo )){ 

			

			$pagos = new AnuncioPagoHelper();

			$args = array(
				'refVenta' => $sku
			,	'description' => 'Compra Anuncio'
			,	'valor' => $costoAnuncio
			,	'iva' => 0
			,	'basevalor' => 0
			,	'comprador_email' => $mail
			,	'currency' => 'COP'
			,	'url_respuesta' => JURI::root().'index.php/anuncio?view=anuncio&layout=response'
			,	'url_confirmacion' => JURI::root().'index.php/anuncio?task=anuncio.responsePayU'
			);

			$pagos->instance( $args );


			$formulario = $pagos->getForm();

			$response->validateCoupon = false;

			$response->form = $formulario;
			$response->status = 200;
			$response->message = 'Anuncio guardado correctamente. Redirigiendo a plataforma de pagos...';

			echo json_encode($response);
			die;

		}else{		

			$dateExpira = date( 'Y-m-d H:i:s', strtotime( '+30 days' ) );
			$this->setDateExpire( $idProducto, $dateExpira );

			$response->validateCoupon = true;
			$response->status = 200;
			$response->message = 'Su anuncio fue creado y se encuentra activo.';

			echo json_encode($response);
			die;
				
		}
		
		

	}

	public function saveImages( $imagenes, $date, $user ){

		$return = new stdClass();
		$return->idsImages = array();



		foreach ($imagenes as $key => $imagen) {
			
			$modelImagen = $this->getModel('medias');			

			$urlImagen = 'imganuncios/tmp/' . $imagen;

			$type = explode( ".", $imagen );

			$type = 'image/' . $type[1];

			$productoImagen = array(
					'file_title' => $imagen
				,	'file_mimetype' => $type
				,	'file_url' => $imagen
				,	'created_on' => $date
				,	'created_by' => $user
				,	'modified_on' => $date
				,	'modified_by' => $user
			);

			$modelImagen->instance( $productoImagen );

			if ( $modelImagen->save('bool')) {

				$pathOficial = 'images/stories/virtuemart/product/' . $imagen;

				rename( $urlImagen, $pathOficial );

				unlink( $urlImagen ); 
			}

			$idImagen = $modelImagen->insertId;
			$return->idsImages[] = $idImagen;

			
		}

		return $return;

	}

	public function mergeImagenes( $idProducto, $imagenes ){

		foreach ($imagenes->idsImages as $key => $imagen) {
			
			$modelImagen = $this->getModel( 'medias' );

			$args = array(
				$idProducto,
				$imagen
			);

			$modelImagen->mergeImagenes( $args );

		}
	}

	protected function validarCodigo( $codigo ){

		$productoModel = $this->getModel( 'producto' );

		$cupon = $productoModel->validarCodigo( $codigo );

		return $cupon;
	}

	protected function setDateExpire( $id, $date ){

		$productoModel = $this->getModel( 'producto' );

		$cupon = $productoModel->setDateExpire( $id, $date );

		return $cupon; 
	}


	public function responsePayU(){

        $metodo_pago = $_REQUEST['payment_method_type'];
		$monto = $_REQUEST['value'];
		$dateTransaction = $_REQUEST['transaction_date'];
		$emailComprador = $_REQUEST['email_buyer'];
		$referenciaVenta = $_REQUEST['reference_sale'];


		if($_REQUEST['state_pol'] == 4){


			$productoModel = $this->getModel( 'producto' );

			$productoModel->publishAnuncio( $referenciaVenta );

			$message = "La transacción se realizó correctamente";

		}else{
			$message = "Hubo un error cuando se trato de realizar la transacción";
		}

        $mail = JFactory::getMailer();

        $contenido = "<div><img src='".JURI::root()."images/header-mail.jpg'><br><br><br><br>";
        $contenido .= "Fecha de transacción: ".$dateTransaction."<br>";
        $contenido .= "Correo electronico del comprador: ".$emailComprador."<br>";
       	$contenido .= "Monto Pagado: ".$monto."<br>";
       	$contenido .= "Observaciones: ".$message."<br>";
        $contenido .= "<img src='".JURI::root()."images/footer-mail.jpg'></div>";

        $mail->setSender( 'info@mcsmaquinarias.com' );
        $mail->addRecipient( $emailComprador );
        $mail->setSubject( "Usted ha realizado una transacción a través de PayU" );
        $mail->Encoding = 'base64';
        $mail->isHtml( true );
        $mail->setBody( $contenido );


        // enviar email
        $mail->Send();
        
   	}

   	public function despublicarAnuncio(){


   		$cid = JRequest::getVar( 'data' );
   		$motivo = JRequest::getVar( 'motivo' );
   		$productos = array();

   		$response = new stdClass();
		
		foreach ($cid as $key => $id) {
			
			$model = $this->getModel( 'producto' );
			$anuncio = $model->getAnuncioById( $id );
			array_push( $productos, $anuncio );


		}

		// get user
		$user = JFactory::getUser();
		$idUser = $user->get('name');

		// get mail from admin
		$app = JFactory::getApplication();
		$mailAdmin = $app->getCfg('mailfrom');


		$mail = JFactory::getMailer();

        $contenido = "<div><img src='".JURI::root()."images/header-mail.jpg'><br><br><br><br>";
        $contenido .= "El usuario " . $idUser . " ha solicitado la despublicación de los siguientes anuncios.<br><br>";
        foreach ($productos as $key => $producto) {
     
        	$contenido .= "Id del anuncio: ". $producto->virtuemart_product_id . "<br>";
        	$contenido .= "Nombre del anuncio: ". $producto->product_name . "<br><br>";

        }
       	
        $contenido .= "<br>Motivo de despublicación: ".$motivo."<br>";
        $contenido .= "<img src='".JURI::root()."images/footer-mail.jpg'></div>";


        $mail->setSender( $emailComprador );
        $mail->addRecipient( $mailAdmin );
        $mail->setSubject( "Han solicitado despublicar un anuncio" );
        $mail->Encoding = 'base64';
        $mail->isHtml( true );
        $mail->setBody( $contenido );


        // enviar email
       	if ( ! $mail->Send()) {
       		$response->status = 500;
			$response->message = 'La solicitud de despublicación no pudo ser enviada.';
       	}

		$response->status = 200;
		$response->message = 'Su solicitud de despublicar anuncio se ha enviado correctamente.';

		echo json_encode($response);
		die;
   	}

   	public function validateExpire(){

   		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->update( '#__virtuemart_products' );
		$query->set( 'published = 0 ' );
		$query->where( 'fecha_expira <= NOW()' );
		
		$db->setQuery( $query );
		
		return $db->execute();
   	}

	
}
?>