<?php
defined( '_JEXEC' ) or die( 'Restricted access' );


$user = JFactory::getUser();
$app = JFactory::getApplication();

if( $user->guest )
	$app->redirect( 'index.php/component/users/?view=login', 'Inicie sesión por favor.' , 'notice');

require_once( JPATH_COMPONENT . DS . 'controller.php' );
require_once( JPATH_COMPONENT . DS . 'helpers' . DS . 'uploader.php' );
require_once( JPATH_COMPONENT . DS . 'helpers' . DS . 'misc.php' );
require_once( JPATH_COMPONENT . DS . 'helpers' . DS . 'pagos.php' );
require_once( JPATH_COMPONENT . DS . 'models' . DS . 'default.php' );

$controller = JControllerLegacy::getInstance('anuncio');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();


?>