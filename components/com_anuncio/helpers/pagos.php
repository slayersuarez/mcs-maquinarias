<?php

/**
 * Model for "Pagos"
 * 
 */

// Initializes the Class
class AnuncioPagoHelper {
	
	/**
	 * Object Id
	 * @var int
	 */
	protected $merchantId = "500238";

	/**
	 * Llave de api: id unico que se obtiene del proveedor de pagos
	 * @var string
	 */
	protected $ApiKey = "6u39nqhq8ftd0hlvnjfs66eh8c";

	/**
	 * Llave de api: id unico que se obtiene del proveedor de pagos
	 * @var string
	 */
	protected $accountId = "500538";

	/**
	 * Referencia de venta: un codigo unico de venta
	 * @var string
	 */
	var $refVenta;

	/**
	 * Descripcion de la compra
	 * @var string
	 */
	var $description = 'Compra de anuncio.';

	/**
	 * Valor de la compra
	 * @var string
	 */
	var $valor;

	/**
	 * Iva de la compra
	 * @var string
	 */
	var $iva;

	/**
	 * valor base de la compra
	 * @var string
	 */
	var $basevalor = 0;

	/**
	 * Email del comprador
	 * @var string
	 */
	var $comprador_email;

	/**
	 * Email del vendedor
	 * @var string
	 */
	var $vendedor_email;

	/**
	 * Moneda de pago
	 * @var bool
	 */
	var $currency = 'COP';

	/**
	 * Firma
	 * @var string
	 */
	protected $signature;

	/**
	 * Firma codificada
	 * @var string
	 */
	protected $enc_signature;

	/**
	* Url de pagos
	* @var string
	*/
	//protected $url_pagos = "https://gateway.payulatam.com/ppp-web-gateway/";
	protected $url_pagos = "https://stg.gateway.payulatam.com/ppp-web-gateway";

	/**
	* Url de respuesta
	* @var string
	*/
	var $url_respuesta = "";

	/**
	* Url de confirmacion
	* @var string
	*/
	var $url_confirmacion = "";
	
	/**
	 * Get if the object exists
	 * @var bool
	 */
	var $exists = false;
	
	/**
	 * Cache of results, data, queries
	 * @var unknown
	 */
	var $data;

	
	/**
	 * Attributes Map
	 * @var array
	 */
	var $attrs_map = array(
			
			'refVenta'
		,	'description'
		,	'valor'
		,	'iva'
		,	'basevalor'
		,	'comprador_email'
		,	'currency'
		,	'url_respuesta'
		,	'url_confirmacion'
	);
	
	
	/**
	 * Methods
	 * 
	 */
	
	/**
	 * Constructor
	 * 
	 * @param { array || int } the args to instance the model or the single id
	 * 
	 */
	public function instance( $config = NULL ){
		
		if( ! is_array( $config ) )
			return;
		
		// Get existing object if the id was passed through
		return $this->fill( $config );
	}
	
	/**
	 * Fill the model attributes with the passed arguments.
	 *
	 * @param { arr } Object arguments
	 */
	protected function fill( $args = null ){


		if ( ! is_array( $args ) )
			return false;

	
		// Merge attributes	when id is not passed through
		foreach ( $this->attrs_map as $attr ) {
			if ( isset( $args[ $attr ] ) )
				$this->$attr = $args[ $attr ];
		}

		// Set exists to true.
		$this->exists = true;
	
	}

	/**
	* Set the signature
	*
	*/
	protected function setSignature(){

		$this->signature = "$this->ApiKey~$this->merchantId~$this->refVenta~$this->valor~$this->currency";
		$this->enc_signature = md5( $this->signature );
	}

	/**
	* Get the form with the data on it.
	*
	*/
	public function getForm(){

		// Set the signature
		$this->setSignature();

		$form = '<form name="form-pagos" id="form-pagos" method="post" action="'.$this->url_pagos.'" target="_self">
			<input type="hidden" name="merchantId" value="'. $this->merchantId .'">
			<input type="hidden" name="merchantId" value="'. $this->accountId .'">
			<input type="hidden" name="referenceCode" value="'. $this->refVenta .'">
			<input type="hidden" name="description" value="'. $this->description .'">
			<input type="hidden" name="amount" value="'. $this->valor .'">
			<input type="hidden" name="tax" value="'. $this->iva .'">
			<input type="hidden" name="taxReturnBase" value="'. $this->basevalor .'">
			<input type="hidden" name="signature" value="'. $this->enc_signature .'">
			<input type="hidden" name="currency" value="'. $this->currency .'">
			<input type="hidden" name="buyerEmail" value="'. $this->comprador_email .'">    
			<input type="hidden" name="confirmationUrl" value="'. $this->url_confirmacion .'">
			<input type="hidden" name="responseUrl" value="'. $this->url_respuesta .'">
			<input type="hidden" name="test" value="1">
		</form>';
	
		return $form;
	}

	/**
	 * API of the class
	 * 
	 * @return { void }
	 */
	
	protected function API(){
		
		// Instance an object with defaults
		$Object = new PagoModel();
		
		// Instance an object with args
		$args = array(
			
				'refVenta' => 'jhaskj'
			,	'description' => 'Mi compra'
			,	'valor' => 20000
			,	'iva' => 0
			,	'basevalor' => 0
			,	'comprador_email' => 'comprador@example.com'
			,	'currency' => 'COP'
			,	'url_respuesta' => 'http://'
			,	'url_confirmacion' => 'http://'
		);
		
		$Object = new PagoModel();
		$Object->instance( $args );
		$Object->getForm(); // Obtiene el formulario de pagos
		
		
	}
}
?>