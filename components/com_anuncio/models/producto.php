<?php
/**
 * @author Sainet Ingenieria Ltda.
 * @author Robinson Perdomo
 * @link http://creandopaginasweb.com
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

/**
 * AdWizard Anuncios Model
 *
 * @package		Joomla.Administrator
 * @subpackage	com_anuncios
 */

jimport('joomla.application.component.model');

// Initializes the Class
class AnuncioModelProducto extends AnunciosModelDefault{


	/**
	 * Object Id
	 * @var int
	 */
	protected $virtuemart_product_id;
	/**
	* Attribute
	* @var Int
	*/
	protected $virtuemart_vendor_id;
	/**
	* Attribute
	* @var Int
	*/
	protected $product_parent_id;
	/**
	* Attribute
	* @var String
	*/
	protected $product_sku;
	/**
	* Attribute
	* @var String
	*/
	protected $product_weight;
	/**
	* Attribute
	* @var String
	*/
	protected $product_weight_uom;
	/**
	* Attribute
	* @var String
	*/
	protected $product_length;
	/**
	* Attribute
	* @var String
	*/
	protected $product_width;
	/**
	* Attribute
	* @var String
	*/
	protected $product_height;
	/**
	* Attribute
	* @var String
	*/
	protected $product_lwh_uom;
	/**
	* Attribute
	* @var String
	*/
	protected $product_url;
	/**
	* Attribute
	* @var String
	*/
	protected $product_in_stock;
	/**
	* Attribute
	* @var String
	*/
	protected $product_ordered;
	/**
	* Attribute
	* @var String
	*/
	protected $low_stock_notificatio;
	/**
	* Attribute
	* @var String
	*/
	protected $product_available_date;
	/**
	* Attribute
	* @var String
	*/
	protected $product_availability;
	/**
	* Attribute
	* @var String
	*/
	protected $product_special;
	/**
	* Attribute
	* @var String
	*/
	protected $product_sales;
	/**
	* Attribute
	* @var String
	*/
	protected $product_unit;
	/**
	* Attribute
	* @var String
	*/
	protected $product_packaging;
	/**
	* Attribute
	* @var String
	*/
	protected $product_params;
	/**
	* Attribute
	* @var String
	*/
	protected $hits;
	/**
	* Attribute
	* @var String
	*/
	protected $intnotes;
	/**
	* Attribute
	* @var String
	*/
	protected $metarobot;
	/**
	* Attribute
	* @var String
	*/
	protected $metaauthor;
	/**
	* Attribute
	* @var String
	*/
	protected $layout;
	/**
	* Attribute
	* @var String
	*/
	protected $published;
	/**
	* Attribute
	* @var String
	*/
	protected $pordering;
	/**
	* Attribute
	* @var Data
	*/
	protected $created_on;
	/**
	* Attribute
	* @var Int
	*/
	protected $created_by;
	/**
	* Attribute
	* @var Data
	*/
	protected $modified_on;
	/**
	* Attribute
	* @var Int
	*/
	protected $modified_by;
	/**
	* Attribute
	* @var String
	*/
	protected $locked_on;
	/**
	* Attribute
	* @var String
	*/
	protected $locked_by;
	/**
	* Attribute
	* @var String
	*/	

	public $table = "#__virtuemart_products";

	/**
	 * Constant for table
	 * @var string
	 */
	const TABLE = '#__virtuemart_products';

	/**
	 * Constant for filters states
	 * @var string
	 */
	const FILTER_STATE = 'adwizard.state.';

	
	/**
	 * Attributes Map
	 * @var array
	 */
	var $attrs_map = array(
			'virtuemart_product_id'
		,	'published'
		,	'virtuemart_vendor_id'
		,	'product_parent_id'
		,	'product_sku'
		,	'product_weight'
		,	'product_weight_uom'
		,	'product_length'
		,	'product_width'
		,	'product_height'
		,	'product_lwh_uom'
		,	'product_url'
		,	'product_in_stock'
		,	'product_ordered'
		,	'low_stock_notificatio'
		,	'product_available_date'
		,	'product_availability'
		,	'product_special'
		,	'product_sales'
		,	'product_unit'
		,	'product_packaging'
		,	'product_params'
		,	'hits'
		,	'intnotes'
		,	'metarobot'
		,	'metaauthor'
		,	'layout'
		,	'pordering'
		,	'created_on'
		,	'created_by'
		,	'modified_on'
		,	'modified_by'
		,	'locked_on'
		,	'locked_by'
	);	
	
	/**
	 * Methods
	 * 
	 */

	public function saveNombre( $args ){


		if( ! is_array( $args ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->insert('#__virtuemart_products_es_es');
		$query->values( $db->quote($args[0]).", ".$db->quote($args[1], true).", ".$db->quote($args[2], true).",". $db->quote($args[3]).",'','','',".$db->quote($args[4] ) );

		$db->setQuery( $query );
		
		return $db->execute();

	}

	public function mergeCategoria( $args ){

		if( ! is_array( $args ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		

		$query->insert('#__virtuemart_product_categories');
		$query->values("'','".$args[0]."','".$args[1]."',''");


		$db->setQuery( $query );
		
		return $db->execute();

	}


	public function mergePrecio( $args ){

		if( ! is_array( $args ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		

		$query->insert('#__virtuemart_product_prices');
		$query->values("'','".$args[0]."','','".$args[1]."','0','0.00000','0','0','31','','','0','0','".$args[2]."','".$args[3]."','".$args[2]."','".$args[3]."','','0'");

		$db->setQuery( $query );
		
		return $db->execute();

	}

	public function publishAnuncio( $sku ){
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->update( '#__virtuemart_products'  );
		$query->set( 'published = 1' );
		$query->where( 'product_sku = '. $db->quote($sku) );
		
		$db->setQuery( $query );
		
		return $db->execute();
	}


	public function validarCodigo( $codigo ){

		if( ! is_string( $codigo ) )
			return false;

		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( '*'  );
		$query->from( '#__virtuemart_coupons' );
		$query->where( 'coupon_code = '. $codigo );
		$query->where( 'NOW() BETWEEN coupon_start_date AND coupon_expiry_date ' );
		
		$db->setQuery( $query );
		
		return $db->loadObject();
	}

	public function setDateExpire( $id, $date ){


		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->update( '#__virtuemart_products'  );
		$query->set( 'fecha_expira = ' . $db->quote($date) );
		$query->where( 'virtuemart_product_id = '. $id );
		
		$db->setQuery( $query );
		
		return $db->execute();
	}

	public function getAnuncioById( $id ){

		if( ! is_string( $id ) )
			return false;

		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( 'a.*, b.*' );
		$query->from( ' #__virtuemart_products AS a');

		$query->innerJoin( '#__virtuemart_products_es_es AS b ON a.virtuemart_product_id = b.virtuemart_product_id ' );

		$query->where( 'a.virtuemart_product_id  = '. $id);

		$db->setQuery( $query );

		return $db->loadObject();

	}
}