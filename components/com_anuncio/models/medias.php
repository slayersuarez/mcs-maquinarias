<?php
/**
 * @author Sainet Ingenieria Ltda.
 * @author Robinson Perdomo
 * @link http://creandopaginasweb.com
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

/**
 * AdWizard Anuncios Model
 *
 * @package		Joomla.Administrator
 * @subpackage	com_anuncios
 */

jimport('joomla.application.component.model');

// Initializes the Class
class AnuncioModelMedias extends AnunciosModelDefault{


	protected $virtuemart_media_id;
		/**
		*	Attribute
		*	@var String
		*/
	protected $virtuemart_vendor_id;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_title;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_description;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_meta;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_mimetype;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_type;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_url;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_url_thumb;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_is_product_image;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_is_downloadable;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_is_forSale;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_params;
		/**
		*	Attribute
		*	@var String
		*/
	protected $file_lang;
		/**
		*	Attribute
		*	@var String
		*/
	protected $shared;
		/**
		*	Attribute
		*	@var String
		*/
	protected $published;
		/**
		*	Attribute
		*	@var String
		*/
	protected $created_on;
		/**
		*	Attribute
		*	@var String
		*/
	protected $created_by;
		/**
		*	Attribute
		*	@var String
		*/
	protected $modified_on;
		/**
		*	Attribute
		*	@var String
		*/
	protected $modified_by;
		/**
		*	Attribute
		*	@var String
		*/
	protected $locked_on;
		/**
		*	Attribute
		*	@var String
		*/
	protected $locked_by;
		/**
		*	Attribute
		*	@var String
		*/

	public $table = "#__virtuemart_medias";

	/**
	 * Constant for table
	 * @var string
	 */
	const TABLE = '#__virtuemart_medias';

	/**
	 * Constant for filters states
	 * @var string
	 */
	const FILTER_STATE = '';

	
	/**
	 * Attributes Map
	 * @var array
	 */
	var $attrs_map = array(
			'virtuemart_media_id'
		,	'virtuemart_vendor_id'
		,	'file_title'
		,	'file_description'
		,	'file_meta'
		,	'file_mimetype'
		,	'file_type'
		,	'file_url'
		,	'file_url_thumb'
		,	'file_is_product_image'
		,	'file_is_downloadable'
		,	'file_is_forSale'
		,	'file_params'
		,	'file_lang'
		,	'shared'
		,	'published'
		,	'created_on'
		,	'created_by'
		,	'modified_on'
		,	'modified_by'
		,	'locked_on'
		,	'locked_by'
	);	
	
	/**
	 * Methods
	 * 
	 */

	public function mergeImagenes( $args ){

		if( ! is_array( $args ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		

		$query->insert('jos_virtuemart_product_medias');
		$query->values("'','".$args[0]."','".$args[1]."',''");

		$db->setQuery( $query );
		
		return $db->execute();

	}

}