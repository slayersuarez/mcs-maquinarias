/**
*
* TecnicoModel for { Tecnico }
*
*/

( function( $, window, document, Utilities ){

	var TecnicoModel = function( a ){

	};

	TecnicoModel.prototype = {


			/**
			* Triggers a method in backend, sending data and returning the response
			*
			*/
			parseExcel: function( success, error, data ){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
					,	error: error
				}
				
				,   aData = {
						option: "com_certificados"
					,	task: "carga.parseExcel"
					,   data: data
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );
				
			}

			/**
			* Truncates the data in db
			*
			*/
		,	truncate: function( success, data ){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
				}
				
				,   aData = {
						option: "com_certificados"
					,	task: "carga.truncate"
					,   data: data
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );
			}

			/**
			* Delete all temporary files after all
			*
			*/
		,	deleteTemp: function( success, data ){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
				}
				
				,   aData = {
						option: "com_certificados"
					,	task: "carga.deleteTemp"
					,   data: data
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );

			}

		,	verifyMatricula: function( success, fileName ){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
				}
				
				,   aData = {
						option: "com_certificados"
					,	task: "carga.verifyMatricula"
					,   fileName: fileName
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );

			}

	};

	// Expose to global scope
	window.TecnicoModel = new TecnicoModel();

})( jQuery, this, this.document, this.Misc, undefined );