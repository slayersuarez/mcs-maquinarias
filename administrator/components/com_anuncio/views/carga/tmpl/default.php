<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();

//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document =& JFactory::getDocument();
$document->addScript( '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
$document->addScript( $host.'administrator/components/com_certificados/assets/js/libs/fileuploader/fileuploader.js');
$document->addStyleSheet($host.'administrator/components/com_certificados/assets/js/libs/fileuploader/fileuploader.css');
$document->addStyleSheet($host.'administrator/components/com_certificados/assets/css/style.css');
$document->addScript( $host.'administrator/components/com_certificados/assets/js/misc/misc.js');
$document->addScript( $host.'administrator/components/com_certificados/assets/js/views/tecnico.js');
$document->addScript( $host.'administrator/components/com_certificados/assets/js/models/tecnico.js');
$document->addScript( $host.'administrator/components/com_certificados/assets/js/controllers/tecnico.js');
$document->addScript( $host.'administrator/components/com_certificados/assets/js/handlers/tecnico.js');
$document->addScript( $host.'administrator/components/com_certificados/assets/js/misc/file.js');
?>
<form action="index.php" method="post" name="adminForm" id="adminForm" class="form-validate">

	<fieldset class="adminform">
		<legend>Archivo Excel</legend>
		<p>Asegúrese que su archivo de excel conserva el mismo formato, puesto que los campos adicionales que no coincidan con la base de datos no se guardarán.</p>
		<ul class="excel-list">
		</ul>
		<div id="excel-uploader">       
			<noscript>          
			    <p>habilita javascript en tu navegador para poder subir archivos.</p>
			    <!-- or put a simple form for upload here -->
			</noscript>         
		</div>

		<p class="import-label">Cuando todos los archivos indiquen 100%, haga click en "Importar" para comenzar la importación de datos.</p>
		<a href="#" id="importar-datos">Importar datos</a>
		<p class="truncate-label"></p>
		<ul class="excel-parse-list"></ul>

		<div class="excel-reporte">
			<h3>Reporte</h3>
			<ul>
				<li>Errores en la conversión: <span class="excel-errors">0</span></li>
			</ul>
			<div class="excel-log">
			</div>
		</div>
	</fieldset>

	<fieldset class="adminform">
		<legend>Matrículas Profesionales</legend>

		<p>Cargue todos las matriculas profesionales de los técnicos.</p>
		
		<div id="pdf-uploader">       
			<noscript>          
			    <p>habilita javascript en tu navegador para poder subir archivos.</p>
			</noscript>         
		</div>

		<div class="pdf-upload-status">
			<span class="message">No ha seleccionado ninguna matrícula profesional aún.</span>
			<ul class="pdf-upload-status-list">
				<li>
					<div class="pdf-status-bar">
						<span class="bar-inside"></span>
						<span class="bar-message"></span>
					</div>
				</li>
				<li>
					<span class="pdf-total-title"></span>
					<div class="pdf-total-bar">
						<span class="bar-inside"></span>
						<span class="bar-message"></span>
					</div>
				</li>
				<li>
					<ul class="pdf-error-list"></ul>
				</li>
			</ul>
		</div>

		<div class="pdf-log">
			<div class="errors">Errores: <span class="pdf-errors">0</span></div>
			<div class="correctos">Correctos: <span class="pdf-correctos">0</span></div>
		</div>
	</fieldset>

	<input type="hidden" name="option" value="com_certificados" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="id" value="" />
	
</form>