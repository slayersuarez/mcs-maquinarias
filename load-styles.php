<?php
/**
*
* Parse the Less Doument and Cache it if neccessary
*
*/

// Set default timezone
ini_set( 'date.timezone', 'America/Bogota' );

if ( substr_count( $_SERVER[ 'HTTP_ACCEPT_ENCODING' ], 'gzip' ) )
    ob_start( 'ob_gzhandler' );
else
    ob_start();


// Get the parameters and build an array with it, then
// check for the cached file

// Available kits
$available_kits = array(
    'template', 'colocarAnuncio', 'catalogo'
);

if ( ! isset( $_GET[ 'load' ] ) )
    return;


// Get the get variable
$kit = $_GET[ 'load' ];


if ( ! in_array( $kit, $available_kits ) )
    return;


//--------------------
// Styles sets
//--------------------

// Framework styles
$components = array(
    'fonts',
    'icons',
    'variables',
    'mixins',
    'grid',
    'base',
    'gui',
    'select',
    'bxslider',
    'register',
    'header-component',
    'mianuncio',
    'crear',
    'modal'
);

// Mobile devices styles
$media = array(
    'laptops',
    'tablets',
    'minitablets',
    'smartphones',
    'menu'
);


// Home styles
$home = array( 'home' );
$anuncio = array( 'colocarAnuncio' );
$catalogo = array( 'catalogo' );

// fontawesome style
$fontawesome = array( 
    'less/variables', 
    'less/mixins', 
    'less/path', 
    'less/core', 
    'less/larger',
    'less/fixed-width',
    'less/list',
    'less/bordered-pulled',
    'less/spinning',
    'less/rotated-flipped',
    'less/stacked',
    'less/icons'
);



//--------------------
// Kits
//--------------------

// Kits
$kits = ( object ) array(

    'template' => array_merge( $components, $fontawesome , $home, $media ),
    'colocarAnuncio' => array_merge( $components, $fontawesome, $anuncio, $media ),
    'catalogo' => array_merge( $components, $fontawesome, $catalogo, $media )

);


/**
* Catching
*
* Compares the time the .less file was modified with the
* time the cache file was modified to.
*/
$time = mktime( 0, 0, 0, 21, 5, 1980 );
$cache = "cache/$kit.less";


// Get the last modifyed file
foreach( $kits->$kit as $file ) {

    $file_time = filemtime( 'less/'.add_ext( $file ) );

    if ( $file_time > $time )
        $time = $file_time;

}


if ( file_exists( $cache ) ) {
    $cacheTime = filemtime( $cache );
    if ( $cacheTime < $time ) {
        $time = $cacheTime;
        $recache = true;
    }
    else {
        $recache = false;
    }
}

else {
    $recache = true;
}

if ( ! $recache && isset( $_SERVER[ 'IF-Modified-Since' ] )
    && strtotime( $_SERVER[ 'If-Modified-Since' ] ) >= $time ) {

    header( 'HTTP/1.0 304 Not Modified' );
}

else {
    header( 'Content-type: text/css' );
    header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s', $time ) . ' GMT' );

    if ( $recache ) {
        // require 'libraries/lessc.inc.php';
        // $lc = new Lessc();
        $css = '';

        foreach ( $kits->$kit as $file ) {

            $css .= file_get_contents( 'less/'. $file . '.less' );
        }



        // $lc->setFormatter( "lessjs" );
        // try{

        // // $css = $lc->parse( $css );            
        //   $css = $lc->compile( $css );

        // }catch (exception $e) {

        //   fb( "fatal error: " . $e->getMessage() );

        // }                
        file_put_contents( $cache, $css );
        echo $css;
    }

    else {
        readfile( $cache );
    }
}


/**
* Helper function to add ".less" sufix to files that
* don't have it.
*
* <code>
*
*      // Return "style.less".
*       add_ext( 'style' );
*
*       // Return "style.less".
*       add_ext( 'style.less' );
*
*       // Return "style.css".
*       add_ext( 'style.css' );
*
* </code>
*
* @param { str } filename
*/
function add_ext( $file ) {


    foreach ( array( '.less', '.css' ) as $ext )
        if ( strstr( $file, $ext ) !== false )
            return $file;

    return $file . '.less';

}

?>