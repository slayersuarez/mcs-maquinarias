(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 380,
	height: 77,
	fps: 32,
	color: "#FFFFFF",
	manifest: []
};



// symbols:



(lib.u = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#525252").ss(0.8).p("ABHhAQADAAgBADIgWBHQgHAcgQANQgOAOgTAAIgkAAQgVAAgGgOQgIgOAIgbIAVhHQAAgDADAAIANAAQADAAgBADIgWBJQgEAPAFAKQAEAIALAAIAjAAQAKAAAJgIQAKgKAEgPIAWhJQABgDACAAg");
	this.shape.setTransform(7.4,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#525252").s().p("AgpBBQgVAAgGgOQgIgOAIgaIAVhIQAAgBAAAAQABgBAAAAQAAAAABAAQAAgBABAAIANAAQABAAAAABQABAAAAAAQAAAAAAABQAAAAAAABIgWBJQgEAPAFAKQAEAIALAAIAjAAQAKAAAJgIQAKgKAEgPIAWhJQAAgBAAgBQABAAAAAAQAAgBABAAQAAAAABAAIANAAQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAABIgWBHQgHAcgQANQgOAOgTAAg");
	this.shape_1.setTransform(7.4,6.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,16.8,15.1);


(lib.s2 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#525252").ss(0.8).p("AAygTQgCAAABgDQABgEACgIIgBgHQgDgDgKAAIgyAAQgIAAgFAEQgEAEgDAMQgBACAAADQAAABABABIAEABIAGAAIACABIAnABQALAAAGADQAHADAEAEQAFAEABAKQAAAKgEAMQgJAhgjAAIgxAAQgJAAgGgCQgGgDgCgEQgEgJAHgYQABgDABAAIAOAAQACAAgBADQgEAPACAEQAAACAEABQACAAAGAAIAvAAQAKAAAFgDQAGgFADgMQADgHgEgEQgEgEgHgDQgGgCgKAAIgmgBIgNgDQgHgBABgJQAAgKADgMQAFgPANgLQALgHAOAAIAvAAQATAAAGAIQAGAMgHAWQgBADgCAAg");
	this.shape.setTransform(7,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#525252").s().p("AgsBBQgJAAgGgCQgGgCgCgFQgEgKAHgXQAAgBAAAAQAAgBABAAQAAAAAAAAQABAAAAAAIAOAAQAAAAABAAQAAAAAAAAQAAAAAAABQAAAAAAABQgEAPACADQAAABAAAAQABABAAAAQAAABABAAQABABABAAIAIABIAvAAQAKAAAFgFQAGgEADgMQADgHgEgFQgEgDgHgDQgGgCgKgBIgmgBIgNgCQgHgBABgJQAAgKADgMQAFgQANgKQALgHAOAAIAvAAQATAAAGAIQAGAMgHAWQAAABAAAAQgBABAAAAQgBAAAAAAQAAAAgBAAIgNAAQAAAAgBAAQAAAAAAAAQAAAAAAgBQAAAAAAgBIADgLIgBgHQgDgEgKgBIgyAAQgIAAgFAFQgEAFgDALIgBAFQAAAAAAABQAAAAAAAAQAAABAAAAQABAAAAAAIAEABIAGABIACAAIAnACQALAAAGACQAHADAEAEQAFAEABAKQAAAKgEAMQgJAhgjAAg");
	this.shape_1.setTransform(7,6.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,16,15.1);


(lib.s = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F4B52B").s().p("ABtA1IAMgpIj1AAIgGAVIhFgBIANguQAFgXAYgKQANgFALAAIEWABIAeAFQAdALgFAcQAAADgSA5g");
	this.shape.setTransform(26.7,5.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F4B52B").s().p("AjUB6IARg+IEJAAIAWgEQAXgJAHgbIAQg4IkHAAQgLAAgLgEQgVgJAFgYIANgvIBDgBIgKAkIEGAAIAZAHQAYALgGAZQABABgiByQgDAMgJAMQgTAZgkAAg");
	this.shape_1.setTransform(21.4,22.8);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,46.8,35.1);


(lib.r = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#525252").ss(0.8).p("AA1gKQgCAAABgDQAJgfgaAAIgiAAQgLAAgJAIQgIAHgFAQIgWBMQgBACgCAAIgOAAQgCAAABgCIAWhMQAFgOACgEIAGgKIABAAIAAgBQAIgLAKgGQAKgFAHAAIArAAQATAAAIANQAIANgIAZQAAADgDAAg");
	this.shape.setTransform(7.4,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#525252").s().p("AhHBBQAAAAgBAAQAAAAAAgBQAAAAAAAAQAAgBAAAAIAXhMIAHgSIAFgKIABAAIAAgBQAIgLAKgFQAKgGAHAAIArAAQATAAAIANQAIAOgIAYQAAABAAABQAAAAgBAAQAAABAAAAQgBAAgBAAIgNAAQAAAAgBAAQAAAAAAgBQAAAAAAAAQAAgBAAgBQAJgfgagBIghAAQgLABgKAHQgHAIgGAQIgWBMQAAAAAAABQgBAAAAAAQAAABgBAAQAAAAgBAAg");
	this.shape_1.setTransform(7.4,6.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,16.7,15.1);


(lib.q = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#525252").ss(0.8).p("AAKBcQgCAAABgDIAQg5QgFAEgHACQgHACgGAAIgkAAQgKAAgHgDQgGgDgEgHQgIgOAHgXIAIgcQAIgaAPgOQAPgNASAAIAlAAQAJAAAIADQAJAEACAGQACADABAFIAAAHIgEAaIglB+QgBADgCAAgAAvgmQAJgggaAAIggAAQgbAAgJAgIgIAcQgJAeAaAAIAgAAQAaAAAKgeg");
	this.shape.setTransform(6.9,9.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#525252").s().p("AAKBcQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAAAAAgBIAQg5QgFAEgHACQgHACgGAAIgkAAQgKAAgHgDQgGgDgEgHQgIgOAHgXIAIgcQAIgaAPgOQAPgNASAAIAlAAQAJAAAIADQAJAEACAGIADAIIAAAHIgEAaIglB+QAAABgBAAQAAABAAAAQgBAAAAABQgBAAAAAAgAgmgmIgIAcQgJAeAaAAIAgAAQAaAAAKgeIAIgcQAJgggaAAIggAAQgbAAgJAgg");
	this.shape_1.setTransform(6.9,9.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2.1,-1,16.9,20.5);


(lib.punto = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CD232E").ss(0.8).p("AgIAIQgBAAAAgBIAEgMQAAgCACAAIAMAAQACAAgBACIgEAMQgBABgBAAg");
	this.shape.setTransform(1,0.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CD232E").s().p("AgIAIQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAAAAAAAIAEgNQAAAAAAAAQAAAAABgBQAAAAAAAAQABAAAAAAIAMAAQAAAAAAAAQABAAAAAAQAAABAAAAQAAAAAAAAIgEANQAAAAgBAAQAAAAAAABQAAAAgBAAQAAAAAAAAg");
	this.shape_1.setTransform(1,0.9);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,4.1,3.7);


(lib.o = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CD232E").ss(0.8).p("AAiA0QgPANgTAAIgkAAQgrAAAPgzIAIgbQAIgZAPgNQAHgGAJgEQAJgDAIAAIAmAAQATAAAIANQAEAFAAALQAAAJgEANIgIAbQgIAZgPANgAAvgNQAKgfgaAAIghAAQgaAAgKAfIgIAbQgJAfAaAAIAhAAQAaAAAJgfg");
	this.shape.setTransform(7,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CD232E").s().p("AgkBBQgrAAAPg0IAIgaQAIgZAPgNQAHgGAJgEQAJgDAIAAIAmAAQATAAAIANQAEAFAAALQAAAJgEANIgIAaQgIAagPANQgPANgTAAgAgmgNIgIAaQgJAgAaABIAhAAQAagBAJggIAIgaQAKgfgagBIghAAQgaABgKAfg");
	this.shape_1.setTransform(7,6.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,15.9,15.1);


(lib.n = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#525252").ss(0.8).p("AAfBBQgBAAAAgCIAWhKQAFgQgFgIQgFgJgKAAIgkAAQgJAAgKAJQgJAJgFAPIgWBKQAAACgDAAIgNAAQgCAAAAgCIAWhIQAHgbAQgNQAQgPASAAIAjAAQAUAAAIAPQAHANgIAbIgVBIQgBACgCAAg");
	this.shape.setTransform(7.3,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#525252").s().p("AAfBBQAAAAgBAAQAAAAAAgBQAAAAAAAAQAAgBAAAAIAWhLQAFgPgFgIQgFgJgKAAIgkAAQgJAAgKAJQgJAJgFAOIgWBLQAAAAAAABQgBAAAAAAQAAABgBAAQAAAAgBAAIgNAAQgBAAAAAAQgBAAAAgBQAAAAAAAAQAAgBAAAAIAWhIQAHgaAQgOQAQgPASAAIAjAAQAUAAAIAPQAHANgIAbIgVBIQgBAAAAABQAAAAAAAAQgBABAAAAQgBAAAAAAg");
	this.shape_1.setTransform(7.3,6.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1.3,17.3,15.4);


(lib.mred = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CD232E").ss(0.8).p("ABVBBQgDAAABgCIATg4QAIgYgDgMQgBgIgFgDQgFgEgJAAIgcAAQgUAAgMAOQgEAGgEAJIgVBOQgBACgCAAIgNAAQgCAAAAgCIAUhCQABgLABgFQABgGgCgFQgCgJgGgDIgGgCIghAAQgJAAgHAEQgIADgGAIQgGAIgDAJQgEAHgEANIgPA3QgBACgBAAIgQAAQgBAAAAgCIAWhJQAEgNAGgJQAGgKAIgHQAHgGALgFQALgEAKAAIArABQAGABADADIAAAAIAGAHIADAIQAHgKAKgFQAFgDAHgBIAwgBQARAAAJALQAFAHABAJQABAIgDAMIgYBQQgBACgCAAg");
	this.shape.setTransform(12.9,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CD232E").s().p("ABVBBQgBAAAAAAQgBAAAAgBQAAAAAAAAQAAgBAAAAIATg4QAIgYgDgMQgBgIgFgDQgFgEgJAAIgcAAQgUAAgMAOQgEAGgEAKIgVBNQAAAAgBABQAAAAAAAAQgBABAAAAQAAAAgBAAIgNAAQgBAAAAAAQAAAAgBgBQAAAAAAAAQAAgBAAAAIAUhCIACgQQABgGgCgFQgCgJgGgCIgGgDIghAAQgJAAgHAEQgIAEgGAHQgGAJgDAIIgIAUIgPA3QAAAAAAABQgBAAAAAAQAAABgBAAQAAAAAAAAIgQAAQAAAAgBAAQAAAAAAgBQAAAAAAAAQAAgBAAAAIAWhJQAEgNAGgIQAGgLAIgGQAHgHALgFQALgEAKAAIArABQAGACADADIAAAAIAGAGIADAIQAHgJAKgFQAFgDAHgCIAwgBQARAAAJALQAFAIABAIQABAJgDALIgYBQQAAAAgBABQAAAAAAAAQgBABAAAAQAAAAgBAAg");
	this.shape_1.setTransform(12.9,6.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-3.8,28.6,17.9);


(lib.mgris = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#525252").ss(0.8).p("ABWBBQgDAAABgCIATg4QAHgYgDgMQgBgIgGgDQgFgEgJAAIgcAAQgTAAgMAOQgEAGgDAJQgCAGgFAPIgOA5QgBACgCAAIgOAAQgCAAABgCIAVhSQABgGgCgFQgDgKgFgCIgGgCIghAAQgKAAgHAEQgIAEgFAHQgFAHgEAKQgGAMgCAIIgOA3QgBACgBAAIgQAAQgCAAABgCIAVhJQADgMAGgKQAGgIAJgJQAHgGALgFQALgEAKAAIArABQAFABADADIABAAQADADACAEQADAFABADQAIgKAJgFQAFgDAGgBIAwgBQARAAAKALQAEAGACAKQABAKgDAKIgYBQQgBACgBAAg");
	this.shape.setTransform(12.8,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#525252").s().p("ABWBBQgBAAAAAAQgBAAAAgBQAAAAAAAAQAAgBAAAAIATg4QAHgYgDgMQgBgHgGgEQgFgEgJAAIgcAAQgTAAgMAOQgEAGgDAKIgHAUIgOA5QgBAAAAABQAAAAgBAAQAAABgBAAQAAAAAAAAIgOAAQgBAAAAAAQAAAAgBgBQAAAAAAAAQAAgBABAAIAVhSQABgGgCgFQgDgJgFgCIgGgDIghAAQgKAAgHAEQgIAEgFAHQgFAHgEAKQgGAMgCAIIgOA3QAAAAAAABQgBAAAAAAQAAABgBAAQAAAAAAAAIgQAAQgBAAAAAAQgBAAAAgBQAAAAAAAAQAAgBABAAIAVhJQADgLAGgKQAGgJAJgIQAHgHALgFQALgEAKAAIArABQAFACADADIABAAIAFAGQADAFABADQAIgKAJgEQAFgDAGgCIAwgBQARAAAKALQAEAGACAKQABAKgDAKIgYBQQAAAAAAABQgBAAAAAAQAAABgBAAQAAAAAAAAg");
	this.shape_1.setTransform(12.8,6.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-3.8,27.7,17.9);


(lib.m = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F4B52B").s().p("AhWC5IBSkPIACgRQgBgRgOgCIhJAAIASg+IB2AAQAYACANAMQAMANgFAVIhaFBg");
	this.shape.setTransform(48.1,26.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F4B52B").s().p("AjFDhIB8mSIAOgYQAUgYAeABICkAAIAYAKQAXAPgFAeIhADbIhHAAIA0ilIAAgUQgFgUgZgCIgmAAQgKgBgLAGQgXAKgGAcQhjE3gIAcg");
	this.shape_1.setTransform(19.8,22.5);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,57.3,45.5);


(lib.i2 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#525252").ss(0.8).p("AAOhDQgBAAABgDIAFgSQAAgCACAAIAKAAQACAAAAACIgGASQgBADgBAAgAgeBbQgCAAAAgCIAkh+QAAgCACAAIAKAAQACAAAAACIgjB+QgBACgCAAg");
	this.shape.setTransform(3.3,9.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#525252").s().p("AgfBbQAAAAAAAAQAAAAgBAAQAAgBAAAAQAAAAAAgBIAjh+QABAAAAgBQAAAAAAAAQABAAAAgBQABAAAAAAIALAAQAAAAAAAAQAAABABAAQAAAAAAAAQAAABAAAAIgjB+QgBABAAAAQAAAAAAABQgBAAAAAAQAAAAgBAAgAAOhDQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAAAAAgBIAGgSQAAgBAAAAQAAAAAAgBQABAAAAAAQABAAAAAAIAKAAQABAAAAAAQAAAAAAAAQABABAAAAQAAAAgBABIgFASQAAABgBAAQAAABAAAAQAAAAgBABQAAAAAAAAg");
	this.shape_1.setTransform(3.3,9.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,8.6,20.4);


(lib.i = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#525252").ss(0.8).p("AAOhDQgCAAABgDIAFgSQABgCACAAIAKAAQACAAgBACIgFASQAAADgCAAgAgeBbQgCAAABgCIAjh+QAAgCACAAIALAAQABAAgBACIgiB+QgBACgCAAg");
	this.shape.setTransform(3.3,9.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#525252").s().p("AgeBbQgBAAAAAAQAAAAAAAAQAAgBAAAAQAAAAAAgBIAjh+QAAAAAAgBQAAAAABAAQAAAAAAgBQAAAAABAAIALAAQAAAAAAAAQAAABAAAAQAAAAAAAAQAAABAAAAIgiB+QAAABgBAAQAAAAAAABQAAAAgBAAQAAAAgBAAgAAOhDQAAAAgBAAQAAgBAAAAQAAAAAAgBQAAAAAAgBIAFgSQAAgBABAAQAAAAAAgBQAAAAABAAQAAAAABAAIAKAAQABAAAAAAQAAAAAAAAQAAABAAAAQAAAAAAABIgFASQAAABAAAAQgBABAAAAQAAAAAAABQgBAAAAAAg");
	this.shape_1.setTransform(3.3,9.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,8.6,20.4);


(lib.enlacelogo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0066CC").s().p("A9FFUIAAqnMA6LAAAIAAKng");
	this.shape.setTransform(186.2,34);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.cred = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CD232E").ss(0.8).p("AA4ARIgBACQgNAugqAAIgoAAQgnAAAOgwIAKgfQADgKAFgKQAGgKAIgHQAQgNARAAIAnAAQATAAAHALQAIAMgHAXQgBADgBAAIgOAAQgBAAAAgDIAAAAQAIgagYAAIgnAAQgLAAgIAIQgIAHgFAPIgJAfQgDAMADAIQAEAIAJAAIAoAAQAYAAAIgaIAAAAQAAgCACAAIAPAAg");
	this.shape.setTransform(7,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CD232E").s().p("AgoBBQgnAAAOgxIAKgdQADgMAFgJQAGgKAIgGQAQgOARAAIAnAAQATAAAHALQAIAMgHAXQAAABAAABQgBAAAAAAQAAABgBAAQAAAAAAAAIgOAAQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAgBAAgBIAAAAQAIgbgYAAIgnAAQgLABgIAHQgIAIgFAQIgJAdQgDANADAJQAEAHAJABIAoAAQAYAAAIgbIAAAAQAAgBAAAAQAAgBABAAQAAAAAAAAQABgBAAAAIAPAAIABACIgBABQgNAugqAAg");
	this.shape_1.setTransform(7,6.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-2.1,16,16.3);


(lib.c = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F4B52B").s().p("AjECtQgegEgFgUQgCgLADgJIBNkIQAFgYAYgJQANgFALAAIEWAAIAfAGQAdALgGAcQAAACgRA7IhJAAIAMgoIj1AAIg2C4QgFAXATAJQAKAFALAAIEVAAIgQA8g");
	this.shape.setTransform(23.3,17.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,46.7,35);


(lib.a3 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#525252").ss(0.8).p("ABDgQIgIAcQgFAQgFALQgFALgFAFQgGAHgGABQgGACgMAAIg5AAQgeAAAKghIAEgNQADgKAEgJQAEgEAHgFQAGgEAJgBQAJgCAPAAIA4AAIAAgDQADgOgDgFIgBAAQgDgGgJAAIgmAAQgVAAgFATQgBACgCAAIgOAAQgDAAABgCIABgDQAGgSAOgKQAGgDAHgDQAKgCADAAIArAAQASAAAHALQAHAKgGAYgAAkAYIAGgUIhDAAQgJAAgDACQgHADgDALIgDAJQgFAQASAAIAwAAQATAAAGgVg");
	this.shape.setTransform(7.2,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#525252").s().p("AgwBBQgeAAAKgiIAEgMQADgLAEgHQAEgGAHgEQAGgEAJgBQAJgCAPAAIA4AAIAAgEQADgMgDgGIgBAAQgDgGgJAAIgmAAQgVAAgFATQgBABAAAAQAAABgBAAQAAAAgBAAQAAABAAAAIgOAAQgBAAgBgBQAAAAAAAAQAAAAAAgBQAAAAAAgBIABgDQAGgTAOgJQAGgEAHgCIANgCIArAAQASAAAHALQAHALgGAWIgCAEIgIAcQgFAQgFALQgFALgFAGQgGAGgGABQgGACgMAAgAglAGQgHACgDAMIgDAJQgFARASAAIAwAAQATgBAGgVIAGgVIhDAAQgJABgDACg");
	this.shape_1.setTransform(7.2,6.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1.2,-1,17.6,16.1);


(lib.a2 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#525252").ss(0.8).p("ABEgTIgJAfQgJAXgBAEQgEAJgGAHQgGAHgGABQgGACgMAAIg5AAQgeAAAKghIADgNIAIgTQAEgEAGgFQAIgEAIgBQAJgCAOAAIA4AAIABgDQADgOgDgFIgBAAQgEgGgJAAIglAAQgVAAgGATQgBACgBAAIgOAAQgDAAABgCIABgDQAGgSAOgKQAFgDAIgDQAKgCADAAIArAAQASABAHAKQAHAJgHAZgAAkAYIAGgUIhDAAQgJAAgDACQgHADgDALIgDAJQgFAQASAAIAwAAQASAAAHgVg");
	this.shape.setTransform(7.2,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#525252").s().p("AgwBBQgeAAAKgiIADgMIAIgSQAEgFAGgFQAIgEAIgBQAJgCAOAAIA4AAIABgEQADgMgDgGIgBAAQgEgGgJAAIglAAQgVAAgGATQAAABAAAAQgBABAAAAQAAAAgBAAQAAABAAAAIgOAAQgBAAAAgBQgBAAAAAAQAAAAAAgBQAAAAAAgBIABgDQAGgTAOgJQAFgEAIgCIANgCIArAAQASABAHAKQAHAJgHAYIgJAgIgKAbQgEAJgGAIQgGAFgGACQgGACgMAAgAglAGQgHACgDAMIgDAJQgFARASAAIAwAAQASgBAHgVIAGgVIhDAAQgJABgDACg");
	this.shape_1.setTransform(7.2,6.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,16.5,15.1);


(lib.a = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#525252").ss(0.8).p("ABDgQIgCAIIgQAvQgEAKgGAGQgGAHgGABQgGACgMAAIg5AAQgeAAAKghIADgNQAEgMAEgHQADgEAHgFQAHgEAJgBQAIgCAPAAIA4AAIABgDQADgMgEgHIAAAAQgEgGgJAAIgmAAQgUAAgGATQgBACgCAAIgOAAQgCAAABgCIABgDQAFgSAOgKQAGgDAIgDQAJgCAEAAIAqAAQASABAIAKQAHAKgHAYgAAjAYIAHgUIhDAAQgKAAgCACQgIADgDALIgCAJQgFAQASAAIAwAAQASAAAGgVg");
	this.shape.setTransform(7.2,6.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#525252").s().p("AgwBBQgeAAAKgiIADgMQAEgMAEgGQADgFAHgFQAHgEAJgBQAIgCAPAAIA4AAIABgEQADgLgEgHIAAAAQgEgGgJAAIgmAAQgUAAgGATQAAABAAAAQgBABAAAAQAAAAgBAAQAAABgBAAIgOAAQAAAAgBgBQAAAAAAAAQAAAAAAgBQAAAAAAgBIABgDQAFgTAOgJQAGgEAIgCIANgCIAqAAQASABAIAKQAHALgHAWIgBAEIgCAJIgQAuQgEAKgGAHQgGAFgGACQgGACgMAAgAglAGQgIACgDAMIgCAJQgFARASAAIAwAAQASgBAGgVIAHgVIhDAAQgKABgCACg");
	this.shape_1.setTransform(7.2,6.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,17.4,16);


(lib.letrasamarillas = function() {
	this.initialize();

	// Capa 1
	this.instance = new lib.s();
	this.instance.setTransform(119.7,28.4,1,1,0,0,0,23.4,17.5);

	this.instance_1 = new lib.c();
	this.instance_1.setTransform(76.6,28.3,1,1,0,0,0,23.3,17.4);

	this.instance_2 = new lib.m();
	this.instance_2.setTransform(28.7,22.7,1,1,0,0,0,28.7,22.7);

	this.addChild(this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,144.1,51);


// stage content:
(lib.logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_130 = function() {
		/* Hacer clic para ir a página Web
		Al hacer clic en la instancia del símbolo especificado, la dirección URL se carga en una nueva ventana del navegador.
		
		Instrucciones:
		1. Reemplace http://www.adobe.com por la dirección URL que desee.
		      Conserve las comillas ("").
		*/
		
		this.button_1.addEventListener("click", fl_ClickToGoToWebPage);
		
		function fl_ClickToGoToWebPage() {
			window.location.href="index.php";
		}
	}
	this.frame_279 = function() {
		this.gotoAndPlay(102)
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(130).call(this.frame_130).wait(149).call(this.frame_279).wait(1));

	// enlace
	this.button_1 = new lib.enlacelogo();
	this.button_1.setTransform(189.7,39.5,1,1,0,0,0,186.2,34);
	this.button_1._off = true;
	new cjs.ButtonHelper(this.button_1, 0, 1, 2, false, new lib.enlacelogo(), 3);

	this.timeline.addTween(cjs.Tween.get(this.button_1).wait(130).to({_off:false},0).wait(150));

	// Capa 3
	this.instance = new lib.letrasamarillas("synched",0);
	this.instance.setTransform(81.5,37,1,1,0,0,0,71.5,22.9);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(130).to({_off:false},0).to({regY:23,scaleX:0.83,scaleY:0.83,y:37.1},19,cjs.Ease.get(1)).to({regY:22.9,scaleX:1,scaleY:1,y:37},23,cjs.Ease.get(1)).wait(108));

	// s
	this.instance_1 = new lib.s();
	this.instance_1.setTransform(152.2,42.5,1,1,0,0,0,23.4,17.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(20).to({_off:false},0).to({x:129.7,alpha:1},9).to({_off:true},102).wait(149));

	// c
	this.instance_2 = new lib.c();
	this.instance_2.setTransform(114.1,42.4,1,1,0,0,0,23.3,17.4);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(10).to({_off:false},0).to({x:86.6,alpha:1},9).to({_off:true},112).wait(149));

	// m
	this.instance_3 = new lib.m();
	this.instance_3.setTransform(66.2,36.8,1,1,0,0,0,28.7,22.7);
	this.instance_3.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({x:38.7,alpha:1},9,cjs.Ease.get(1)).to({_off:true},122).wait(149));

	// m red
	this.instance_4 = new lib.mred();
	this.instance_4.setTransform(362.6,50.3,1,1,0,0,0,13.6,4.8);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(95).to({_off:false},0).to({x:357.6,alpha:1},4).wait(181));

	// o
	this.instance_5 = new lib.o();
	this.instance_5.setTransform(339.9,52.1,1,1,0,0,0,6.6,6.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(92).to({_off:false},0).to({x:335.9,alpha:1},4).wait(184));

	// c red
	this.instance_6 = new lib.cred();
	this.instance_6.setTransform(324.6,51.3,1,1,0,0,0,6.7,5.7);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(89).to({_off:false},0).to({x:320.6,alpha:1},4).wait(187));

	// punto
	this.instance_7 = new lib.punto();
	this.instance_7.setTransform(308.9,57.8,1,1,0,0,0,1.1,0.8);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(86).to({_off:false},0).to({alpha:1},3).wait(191));

	// s2
	this.instance_8 = new lib.s2();
	this.instance_8.setTransform(300.1,57.7,1,0.153,0,0,0,7,6.5);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(81).to({_off:false},0).to({scaleY:1,y:52.1,alpha:1},5).wait(194));

	// a3
	this.instance_9 = new lib.a3();
	this.instance_9.setTransform(285,58.1,1,0.115,0,0,0,7.6,7.4);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(76).to({_off:false},0).to({scaleY:1,y:53,alpha:1},5).wait(199));

	// i2
	this.instance_10 = new lib.i2();
	this.instance_10.setTransform(275.5,57.9,1,0.087,0,0,0,3.3,9.2);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(71).to({_off:false},0).to({scaleY:1,y:49.6,alpha:1},5).wait(204));

	// r
	this.instance_11 = new lib.r();
	this.instance_11.setTransform(265.3,58.1,1,0.099,0,0,0,7.4,6.5);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(66).to({_off:false},0).to({scaleY:1,y:52.1,alpha:1},5).wait(209));

	// a2
	this.instance_12 = new lib.a2();
	this.instance_12.setTransform(250.1,57.9,1,0.13,0,0,0,7.2,7);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(61).to({_off:false},0).to({regY:6.9,scaleY:1,y:52.5,alpha:1},5).wait(214));

	// n
	this.instance_13 = new lib.n();
	this.instance_13.setTransform(234.8,58.2,1,0.069,0,0,0,8,5.8);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(56).to({_off:false},0).to({regY:6,scaleY:1,y:51.6,alpha:1},5).wait(219));

	// i
	this.instance_14 = new lib.i();
	this.instance_14.setTransform(225.4,57.6,1,0.125,0,0,0,3.2,9.2);
	this.instance_14.alpha = 0;
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(51).to({_off:false},0).to({scaleY:1,y:49.6,alpha:1},5).wait(224));

	// u
	this.instance_15 = new lib.u();
	this.instance_15.setTransform(215.1,58.1,1,0.103,0,0,0,7,6.8);
	this.instance_15.alpha = 0;
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(46).to({_off:false},0).to({regY:6.7,scaleY:1,y:52.3,alpha:1},5).wait(229));

	// q
	this.instance_16 = new lib.q();
	this.instance_16.setTransform(198.7,62.8,1,0.119,0,0,0,6.2,9.2);
	this.instance_16.alpha = 0;
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(41).to({_off:false},0).to({scaleY:1,y:54.7,alpha:1},5).wait(234));

	// a
	this.instance_17 = new lib.a();
	this.instance_17.setTransform(185.1,58,1,0.122,0,0,0,8,7.4);
	this.instance_17.alpha = 0;
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(36).to({_off:false},0).to({regY:7.3,scaleY:1,y:52.9,alpha:1},5).wait(239));

	// m gris
	this.instance_18 = new lib.mgris();
	this.instance_18.setTransform(162.9,57.8,1,0.103,0,0,0,12.9,4.9);
	this.instance_18.alpha = 0;
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(30).to({_off:false},0).to({regY:4.8,scaleY:1,y:50.3,alpha:1},6).wait(244));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(161.5,50.6,197,57);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;


