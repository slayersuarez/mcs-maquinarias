jQuery.noConflict();

jQuery(document).ready(function($){
	//Remove formstyle field
	$$("#jform_params_formstyle-lbl").getParent().destroy();

	//Select content source
	var filter_selected = $("#module-form fieldset .ju_filter input:checked").val();
	
	//Joomla 2.5
	$("#module-form fieldset").find(".ju_filter_element").parent("li").hide();
	$("#module-form fieldset").find("."+filter_selected).parent("li").show();
	
	$("#module-form fieldset .ju_filter input").click(function(){
		$("#module-form fieldset").find(".ju_filter_element").parent("li").hide();
		$("#module-form fieldset").find("."+$(this).val()).parent("li").show();
	});
	
	//Joomla 3.0
	$("#module-form fieldset").find(".ju_filter_element").parent().parent(".control-group").hide();
	$("#module-form fieldset").find("."+filter_selected).parent().parent(".control-group").show();
	
	$("#module-form fieldset .ju_filter input").click(function(){
		console.log(this);
		$("#module-form fieldset").find(".ju_filter_element").parent().parent(".control-group").hide();
		$("#module-form fieldset").find("."+$(this).val()).parent().parent(".control-group").show();
	});
});