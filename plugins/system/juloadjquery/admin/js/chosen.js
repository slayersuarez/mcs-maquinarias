jQuery.noConflict();
//Chosen
window.addEvent("domready",function(){
	jQuery(".pane-sliders select").each(function(){
		if(jQuery(this).is(":visible")) {
		jQuery(this).css({width: parseInt(jQuery(this).width())+20, minWidth: "120px"});
		jQuery(this).chosen();
		};
	});
	jQuery(".chzn-container").click(function(){
		jQuery(".panel .pane-slider,.panel .panelform").css("overflow","visible");	
	});
	jQuery(".panel .title").click(function(){
		jQuery(".panel .pane-slider,.panel .panelform").css("overflow","hidden");		
	});
});

